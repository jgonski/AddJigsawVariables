#include "AddJigsawVariables.hh"

using namespace RestFrames;

AddJigsawVariables::AddJigsawVariables(TTree* tree)
                   : SusySkimHiggsinoBase(tree) 
{
  InitJigsawTree();
}

AddJigsawVariables::~AddJigsawVariables()
{
  CleanJigsawTree();
}

void AddJigsawVariables::InitJigsawTree()
{
  ////////////// Tree set-up /////////////////
  LAB = new LabRecoFrame("LAB","lab");
  CM = new DecayRecoFrame("CM","CM");
  S = new DecayRecoFrame("S","S");
  ISR = new VisibleRecoFrame("ISR","ISR");
  V = new VisibleRecoFrame("V","Vis");
  L = new VisibleRecoFrame("L","Lep");
  I = new InvisibleRecoFrame("I","Inv");

  LAB->SetChildFrame(*CM);
  CM->AddChildFrame(*ISR);
  CM->AddChildFrame(*S);
  S->AddChildFrame(*V);
  S->AddChildFrame(*I);
  S->AddChildFrame(*L);

  LAB->InitializeTree(); 

  ////////////// Tree set-up /////////////////

  ////////////// Jigsaw rules set-up /////////////////
  INV = new InvisibleGroup("INV","Invisible System");
  INV->AddFrame(*I);

  VIS = new CombinatoricGroup("VIS","Visible Objects");
  VIS->AddFrame(*ISR);
  VIS->SetNElementsForFrame(*ISR,1,false);
  VIS->AddFrame(*V);
  VIS->SetNElementsForFrame(*V,0,false);

  // set the invisible system mass to zero
  InvMass = new SetMassInvJigsaw("InvMass", "Invisible system mass Jigsaw");
  INV->AddJigsaw(*InvMass);
  
  // define the rule for partitioning objects between "ISR" and "V"
  SplitVis = new MinMassesCombJigsaw("SplitVis","Minimize M_{ISR} and M_{S} Jigsaw");
  VIS->AddJigsaw(*SplitVis);
  // "0" group (ISR)
  SplitVis->AddFrame(*ISR, 0);
  // "1" group (V + I + L)
  SplitVis->AddFrame(*V,1);
  SplitVis->AddFrame(*I,1);
  SplitVis->AddFrame(*L,1);
  

  LAB->InitializeAnalysis(); 
   ////////////// Jigsaw rules set-up /////////////////
}

void AddJigsawVariables::CleanJigsawTree()
{
  delete LAB;
  delete CM;
  delete S;
  delete ISR;
  delete V;
  delete I;
  delete L;
  delete INV;
  delete InvMass;
  delete VIS;
  delete SplitVis;
}

void AddJigsawVariables::AddNewBranches()
{
  m_b_nJets30 = fChain->Branch("RJR_nJets30", &m_nJets30, "RJR_nJets30/I");
  m_b_nBJets = fChain->Branch("RJR_nBJets", &m_nBJets, "RJR_nBJets/I");
  m_b_PTISR = fChain->Branch("RJR_PTISR", &m_PTISR, "RJR_PTISR/D");
  m_b_RISR = fChain->Branch("RJR_RISR", &m_RISR, "RJR_RISR/D");
  m_b_MS = fChain->Branch("RJR_MS", &m_MS, "RJR_MS/D");
  m_b_dphiISRI = fChain->Branch("RJR_dphiISRI", &m_dphiISRI, "RJR_dphiISRI/D");
  m_b_dphiSL1 = fChain->Branch("RJR_dphiSL1", &m_dphiSL1, "RJR_dphiSL1/D");
  m_b_cosIL1 = fChain->Branch("RJR_cosIL1", &m_cosIL1, "RJR_cosIL1/D");
  m_b_NbV = fChain->Branch("RJR_NbV", &m_NbV, "RJR_NbV/I");
  m_b_NjV = fChain->Branch("RJR_NjV", &m_NjV, "RJR_NjV/I");
  m_b_NlV = fChain->Branch("RJR_NlV", &m_NlV, "RJR_NlV/I");
  m_b_NjISR = fChain->Branch("RJR_NjISR", &m_NjISR, "RJR_NjISR/I");
  m_b_pTbV1 = fChain->Branch("RJR_pTbV1", &m_pTbV1, "RJR_pTbV1/D");
}

void AddJigsawVariables::ProcessFile() 
{
 if( fChain !=NULL ) {
   Long64_t N = fChain->GetEntries();

   // only load relevant input branches
   fChain->SetBranchStatus("*",1);
   //fChain->SetBranchStatus("n_el",1);
   //fChain->SetBranchStatus("n_mu",1);
   //fChain->SetBranchStatus("n_lep",1);
   //fChain->SetBranchStatus("n_jet",1);
   //fChain->SetBranchStatus("*lep*",1);
   //fChain->SetBranchStatus("mu_*",1);
   //fChain->SetBranchStatus("lep_*",1);
   //fChain->SetBranchStatus("*jet*",1);
   //fChain->SetBranchStatus("EtMiss*",1);
   //fChain->SetBranchStatus("met_x",1);
   //fChain->SetBranchStatus("met_y",1);

   // creating the new branches
   AddNewBranches();
   for(Long64_t i = 0; i < N; i++){
     int mymod = N/10;
     if(mymod < 1)
       mymod = 1;
     if(i%mymod == 0)
       cout << " event = " << i << " : " << N << endl;
     fChain->GetEntry(i);
     ComputeJigsawVariables();
     // fill output branches
     m_b_nJets30->Fill();
     m_b_nBJets->Fill();
     m_b_PTISR->Fill();
     m_b_RISR->Fill();
     m_b_MS->Fill();
     m_b_dphiISRI->Fill();
     m_b_dphiSL1->Fill();
     m_b_cosIL1->Fill();
     m_b_NbV->Fill();
     m_b_NjISR->Fill();
     m_b_NlV->Fill();
     m_b_pTbV1->Fill();
   }
   fChain->SetBranchStatus("*",1);
   fChain->Write();//"",TObject::kOverwrite);
 } else {
   cout << "ERROR - AddJigsawVariables::ProcessFile() => Can't load input tree, will do nothing." << endl;
 }
}

TVector3 AddJigsawVariables::GetMET(){
  TVector3 met;
  double exmiss = EtMiss_truth * cos(EtMiss_truthPhi);   //2tipi di etmiss, questa da tracce e calorimetro, l'altra solo dal calorimetro
  double eymiss = EtMiss_truth * sin(EtMiss_truthPhi);
  met.SetXYZ(exmiss/1000., eymiss/1000., 0.0);
  //met.SetPtEtaPhi(EtMiss_truth/1000., 0.0, EtMiss_truthPhi/1000.);
  return met;
}

void AddJigsawVariables::GetJets(vector<Jet>& JETs, double pt_cut, double eta_cut, double btag_WP_cut){
  JETs.clear();

  for(int i = 0; i < ptjets->size(); i++){
    Jet JET;
    JET.P.SetPtEtaPhiM( ptjets->at(i)/1000., etajets->at(i), phijets->at(i), massjets->at(i)/1000. );
    if((JET.P.Pt() >= pt_cut) && (fabs(JET.P.Eta()) < eta_cut || eta_cut < 0)){
      if(isB77jets->at(i)>btag_WP_cut)
        JET.btag = true;
      else
        JET.btag = false;
      JETs.push_back(JET);
    }
  }
}

//void AddJigsawVariables::GetMuons(vector<TLorentzVector>& MUs, double pt_cut, double eta_cut) {
//  MUs.clear();
//  for(int i = 0; i < n_mu; i++){
//    TLorentzVector MU;
//    MU.SetPtEtaPhiE(mu_pt[i]/1000., mu_eta[i],
//                    mu_phi[i], mu_e[i]/1000.);
//    if((MU.Pt() >= pt_cut) && (fabs(MU.Eta()) < eta_cut || eta_cut < 0)){
//      MUs.push_back(MU);
//    }
//  }
//}
//
//void AddJigsawVariables::GetElectrons(vector<TLorentzVector>& ELs, double pt_cut, double eta_cut) {
//  ELs.clear();
//  for(int i = 0; i < n_el; i++){
//    TLorentzVector EL;
//    EL.SetPtEtaPhiE(el_pt[i]/1000., el_eta[i],
//		    el_phi[i], el_e[i]/1000.);
//    if((EL.Pt() >= pt_cut) && (fabs(EL.Eta()) < eta_cut || eta_cut < 0)){
//      ELs.push_back(EL);
//    }
//  }
//}

void AddJigsawVariables::GetLeptons(vector<TLorentzVector>& LEPs, vector<int>& IDs, double pt_cut, double eta_cut) {
  LEPs.clear();
  IDs.clear(); 

  for(int i = 0; i < ptleptons->size(); i++){
    TLorentzVector LEP;
    LEP.SetPtEtaPhiM(ptleptons->at(i) / 1000., etaleptons->at(i),
                     phileptons->at(i), massleptons->at(i) / 1000.);
     //LEP.SetPtEtaPhiE(lep_pt->at(i), lep_eta->at(i),
     //               lep_phi->at(i), lep_e->at(i));
    if((LEP.Pt() >= pt_cut) && (fabs(LEP.Eta()) < eta_cut || eta_cut < 0)){
      LEPs.push_back(LEP);
      IDs.push_back(flavlep->at(i));
    }
  }

}

void AddJigsawVariables::ComputeJigsawVariables()
{
  // only compute for MET>230 to speed-up things
  //if(EtMiss_truth < 150000) return;

  //  B-tagging WP -> MV2c10
  //  double btag_cut = 0.6459; // 77% working point
  vector<Jet> Jets; 
  //GetJets(Jets, 25., 2.8, 0.6459); 
  GetJets(Jets, 30, 2.8, -1); 

  //vector<Jet> Jets30;
  //GetJets(Jets30, 30., 2.8, 0.6459);
  if (Jets.size() < 1) //REQUIRE THIS IF WE ARE FORCING LEPTONS
    return;

  vector<TLorentzVector> Leptons;
  vector<int> LepIDs;
  GetLeptons(Leptons,LepIDs); 

  //vector<TLorentzVector> Muons; 
  ///GetMuons(Muons); 
  
  //vector<TLorentzVector> Elecs; 
  //GetElectrons(Elecs); 

  // need two objects to play
  if(Jets.size() + Leptons.size() < 2) 
    return; 

  TVector3 ETMiss = GetMET();

  //// need at least two jets to play...
  //if(Jets.size() < 2) 
  //  return; 
  
  // count b-jets (with custom b-tagging WP)
  int nBJets = 0;
  for( int i=0; i< int(Jets.size()); i++ ) {
    if ( Jets[i].btag == true ) nBJets++;
  }
  
  m_nJets30 = Jets.size();
  m_nBJets = nBJets;

  // analyze event in RestFrames tree
  LAB->ClearEvent();
  
  m_HT = 0.;
  vector<RFKey> jetID; 
  for(int i = 0; i < int(Jets.size()); i++){
    TLorentzVector jet = Jets[i].P;
    // transverse view of jet 4-vectors
    jet.SetPtEtaPhiM(jet.Pt(),0.0,jet.Phi(),jet.M());
    jetID.push_back(VIS->AddLabFrameFourVector(jet));
    m_HT += jet.Pt();
  }

  //  vector<RFKey> lepID;
  TLorentzVector lepSys;
  lepSys.SetPtEtaPhiM(0.0,0.0,0.0,0.0);
  vector<RFKey> lepID; 
  for(int i = 0; i < int(Leptons.size()); i++){
    TLorentzVector lep1;
    // transverse view of mu 4-vectors
    lep1.SetPtEtaPhiM(Leptons[i].Pt(),0.0,Leptons[i].Phi(),Leptons[i].M());
    //std::cout << "Lpton pt: " << Leptons[i].Pt() << ", eta: " << Leptons[i].Eta() << ", m: " << Leptons[i].M() << std::endl;
    lepID.push_back(VIS->AddLabFrameFourVector(lep1));
    lepSys = lepSys + lep1;
  }  
  L->SetLabFrameFourVector(lepSys);
  //vector<RFKey> muID; 
  //for(int i = 0; i < int(Muons.size()); i++){
  //TLorentzVector mu = Muons[i];
  //// transverse view of mu 4-vectors
  // mu.SetPtEtaPhiM(mu.Pt(),0.0,mu.Phi(),mu.M());
  // muID.push_back(VIS->AddLabFrameFourVector(mu));
  //}
  //vector<RFKey> elID; 
  //for(int i = 0; i < int(Elecs.size()); i++){
  //  TLorentzVector el = Elecs[i];
  //  // transverse view of mu 4-vectors
  //  el.SetPtEtaPhiM(el.Pt(),0.0,el.Phi(),el.M());
  //  elID.push_back(VIS->AddLabFrameFourVector(el));
  //}
  INV->SetLabFrameThreeVector(ETMiss);
  std::cout << "ETMiss: " << ETMiss.Mag() << endl;
  if(!LAB->AnalyzeEvent()) cout << "Something went wrong..." << endl;
  // Compressed variables from tree
  vector<TLorentzVector> Btags;
  m_NjV = 0; 
  m_NjISR = 0;
  // assuming pT ordered jets
  for(int i = 0; i < int(Jets.size()); i++){
    if(VIS->GetFrame(jetID[i]) == *V){ // sparticle group
      m_NjV++;
      if(m_NjV == 1)
	m_pTjV1 = Jets[i].P.Pt();
      if(m_NjV == 2)
	m_pTjV2 = Jets[i].P.Pt();
      if(m_NjV == 3)
	m_pTjV3 = Jets[i].P.Pt();
      if(m_NjV == 4)
	m_pTjV4 = Jets[i].P.Pt();
      if(m_NjV == 5)
	m_pTjV5 = Jets[i].P.Pt();
      if(m_NjV == 6)
	m_pTjV6 = Jets[i].P.Pt();
      if(Jets[i].btag){
	m_NbV++;
	Btags.push_back(Jets[i].P);
	if(m_NbV == 1)
	  m_pTbV1 = Jets[i].P.Pt();
	if(m_NbV == 2)
	  m_pTbV2 = Jets[i].P.Pt();
      }
    } else {
      m_NjISR++;
      if(Jets[i].btag){
	m_NbISR++;
	Btags.push_back(Jets[i].P);
      }
    }
  }

  //if(int(Btags.size() >= 2))
  //  m_Mbb = (Btags[0]+Btags[1]).M();
 m_NlV = 0;  
 bool foundOS = false;
  for(int i = 0; i < int(Leptons.size()); i++){
          if(VIS->GetFrame(lepID[i]) == *V) // sparticle group
        	  m_NlV++;
  }
  // assuming pT ordered leptons
  for(int i = 0; i < int(Leptons.size()); i++){
	  for(int j = 0; j < int(Leptons.size()); j++){
		  if (!foundOS) {
			  if (LepIDs[i] == -LepIDs[j]) {
				  std::cout << "Lep1 flav: " << LepIDs[i] << ", lep2 flav:" << LepIDs[j] << std::endl;
				  foundOS = true;
				  TLorentzVector lep1;
				  TLorentzVector lep2;
				  lep1.SetPtEtaPhiM(Leptons[i].Pt(), Leptons[i].Eta(),
						  Leptons[i].Phi(), Leptons[i].M());
				  lep2.SetPtEtaPhiM(Leptons[j].Pt(), Leptons[j].Eta(),
						  Leptons[j].Phi(), Leptons[j].M());
				  //m_mllOS = (lep1 + lep2).M();
				  lep1.Clear();
				  lep2.Clear();
				  lep1.SetPtEtaPhiM(Leptons[i].Pt(), 0.0,
						  Leptons[i].Phi(), max(0.,Leptons[i].M()));
				  lep2.SetPtEtaPhiM(Leptons[j].Pt(), 0.0,
						  Leptons[j].Phi(), max(0.,Leptons[j].M()));
				  TLorentzVector vL1_CM   = CM->GetFourVector(lep1);
				  TLorentzVector vL2_CM   = CM->GetFourVector(lep2);
				  TLorentzVector vL1_S    = S->GetFourVector(lep1);
				  TLorentzVector vL2_S    = S->GetFourVector(lep2);
				  TLorentzVector vL1_L    = L->GetFourVector(lep1);
				  TLorentzVector vL2_L    = L->GetFourVector(lep2);
				  TLorentzVector vCM_lab = CM->GetFourVector();
				  TLorentzVector vS_CM   = S->GetFourVector(*CM);
				  TLorentzVector vI_S    = I->GetFourVector(*S);
				  TLorentzVector vL_S    = L->GetFourVector(*S);
				  //m_MZ = (vL1_S + vL2_S).M();
				  //if (fabs(vL1_S.Phi()) < TMath::Pi())
				  //  m_dphiLLOS = fabs(vL1_L.DeltaPhi(vL2_L));
				  //m_cosLLOS = -vL_S.Vect().Unit().Dot(vL1_L.Vect().Unit());
				  //}
		  }
	  }
  }
}
    m_PTISR = 0.;
    m_PTCM = 0.;
    m_RISR = 0.;
    m_cosS = 0.;
    //m_NjV = 0.;
    m_MS = 0.;
    m_MV = 0.;
    m_ML = 0.;
    m_MISR = 0.;
    m_dphiCMI = 0.;
    m_dphiISRI = 0.;
    m_dphiSL1 = 0.;
    m_cosIL1 = 0.;

  // assuming pT ordered muons
  //for(int i = 0; i < int(Muons.size()); i++){
  //  if(VIS->GetFrame(muID[i]) == *V) // sparticle group
  //    m_NlV++;
  //  else
  //    m_NlISR++;
  //
  //  TLorentzVector lep;
  //  lep.SetPtEtaPhiM(Muons[i].Pt(), 0.0,
  //      	     Muons[i].Phi(), max(0.,Muons[i].M()));
  //  TLorentzVector vL_CM   = CM->GetFourVector(lep);
  //  TLorentzVector vL_S    = S->GetFourVector(lep);
  //  TLorentzVector vCM_lab = CM->GetFourVector();
  //  TLorentzVector vS_CM   = S->GetFourVector(*CM);
  //  TLorentzVector vI_S    = I->GetFourVector(*S);
  //  if(m_NlV+m_NlISR == 1){
  //    m_dphiCML1 = acos( vL_CM.Vect().Unit().Dot(vCM_lab.Vect().Unit()) );
  //    m_dphiSL1 = acos( vL_S.Vect().Unit().Dot(vS_CM.Vect().Unit()) );
  //    m_MW1 = (vL_S + vI_S).M();
  //    TVector3 boostLI = (vL_S + vI_S).BoostVector();
  //    vL_S.Boost(-boostLI); 
  //    m_cosIL1 = -boostLI.Unit().Dot(vL_S.Vect().Unit());
  //    
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dRMinbl1 > Muons[i].DeltaR(Btags[j]) || m_dRMinbl1 < 0.)
  //        m_dRMinbl1 = Muons[i].DeltaR(Btags[j]);
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dphiMinbl1 > fabs(Muons[i].DeltaR(Btags[j])) || m_dphiMinbl1 < 0.)
  //        m_dphiMinbl1 = fabs(Muons[i].DeltaR(Btags[j]));
  //  }
  //  if(m_NlV+m_NlISR == 2){
  //    m_dphiCML2 = acos( vL_CM.Vect().Unit().Dot(vCM_lab.Vect().Unit()) );
  //    m_dphiSL2 = acos( vL_S.Vect().Unit().Dot(vS_CM.Vect().Unit()) );
  //    m_MW2 = (vL_S + vI_S).M();
  //    TVector3 boostLI = (vL_S + vI_S).BoostVector();
  //    vL_S.Boost(-boostLI); 
  //    m_cosIL2 = -boostLI.Unit().Dot(vL_S.Vect().Unit());
  //
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dRMinbl2 > Muons[i].DeltaR(Btags[j]) || m_dRMinbl2 < 0.)
  //        m_dRMinbl2 = Muons[i].DeltaR(Btags[j]);
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dphiMinbl2 > fabs(Muons[i].DeltaR(Btags[j])) || m_dphiMinbl2 < 0.)
  //        m_dphiMinbl2 = fabs(Muons[i].DeltaR(Btags[j]));
  //  }
  //}
  //// assuming pT ordered electrons
  //for(int i = 0; i < int(Elecs.size()); i++){
  //  if(VIS->GetFrame(elID[i]) == *V) // sparticle group
  //    m_NlV++;
  //  else
  //    m_NlISR++;
  //
  //  TLorentzVector lep;
  //  lep.SetPtEtaPhiM(Elecs[i].Pt(), 0.0,
  //      	     Elecs[i].Phi(), max(0.,Elecs[i].M()));
  //  TLorentzVector vL_CM   = CM->GetFourVector(lep);
  //  TLorentzVector vL_S    = S->GetFourVector(lep);
  //  TLorentzVector vCM_lab = CM->GetFourVector();
  //  TLorentzVector vS_CM   = S->GetFourVector(*CM);
  //  TLorentzVector vI_S    = I->GetFourVector(*S);
  //  if(m_NlV+m_NlISR == 1){
  //    m_dphiCML1 = acos( vL_CM.Vect().Unit().Dot(vCM_lab.Vect().Unit()) );
  //    m_dphiSL1 = acos( vL_S.Vect().Unit().Dot(vS_CM.Vect().Unit()) );
  //    m_MW1 = (vL_S + vI_S).M();
  //    TVector3 boostLI = (vL_S + vI_S).BoostVector();
  //    vL_S.Boost(-boostLI); 
  //    m_cosIL1 = -boostLI.Unit().Dot(vL_S.Vect().Unit());
  //
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dRMinbl1 > Elecs[i].DeltaR(Btags[j]) || m_dRMinbl1 < 0.)
  //        m_dRMinbl1 = Elecs[i].DeltaR(Btags[j]);
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dphiMinbl1 > fabs(Elecs[i].DeltaR(Btags[j])) || m_dphiMinbl1 < 0.)
  //        m_dphiMinbl1 = fabs(Elecs[i].DeltaR(Btags[j]));
  //  }
  //  if(m_NlV+m_NlISR == 2){
  //    m_dphiCML2 = acos( vL_CM.Vect().Unit().Dot(vCM_lab.Vect().Unit()) );
  //    m_dphiSL2 = acos( vL_S.Vect().Unit().Dot(vS_CM.Vect().Unit()) );
  //    m_MW2 = (vL_S + vI_S).M();
  //    TVector3 boostLI = (vL_S + vI_S).BoostVector();
  //    vL_S.Boost(-boostLI); 
  //    m_cosIL2 = -boostLI.Unit().Dot(vL_S.Vect().Unit());
  //
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dRMinbl2 > Elecs[i].DeltaR(Btags[j]) || m_dRMinbl2 < 0.)
  //        m_dRMinbl2 = Elecs[i].DeltaR(Btags[j]);
  //    for(int j = 0; j < int(Btags.size()); j++)
  //      if(m_dphiMinbl2 > fabs(Elecs[i].DeltaR(Btags[j])) || m_dphiMinbl2 < 0.)
  //        m_dphiMinbl2 = fabs(Elecs[i].DeltaR(Btags[j]));
  //  }
  //}

  // need at least one jet or lepton associated with sparticle-side of event
  if(m_NjV+m_NlV > 0) { 
    TVector3 vP_ISR = ISR->GetFourVector(*CM).Vect();
    TLorentzVector fV_ISR = ISR->GetFourVector(*CM);
    TVector3 vP_I   = I->GetFourVector(*CM).Vect();
    TVector3 vP_CM   = CM->GetFourVector(*LAB).Vect();
    
    m_PTISR = vP_ISR.Mag();
    m_PTCM  = vP_CM.Pt();
    m_RISR = fabs(vP_I.Dot(vP_ISR.Unit())) / m_PTISR;
    m_cosS = S->GetCosDecayAngle();
    m_MS = S->GetMass();
    m_MV = V->GetMass();
    m_ML = L->GetMass();
    m_MISR = ISR->GetMass();
    m_dphiCMI = acos(-1.)-fabs(CM->GetDeltaPhiBoostVisible());
    m_dphiISRI = fabs(vP_ISR.DeltaPhi(vP_I));
    if(m_MS < 100) std::cout << "RISR: " << m_RISR << ", PTISR: " << m_PTISR << ", MS: " << m_MS << ", invisible: " << vP_I.Mag() << ", NjV: " << m_NjV << ", NjISR: " << m_NjISR << ", NlV: " << m_NlV << ", MV:" << m_MV << ", ML: " << m_ML << std::endl;
  //for(int i = 0; i < int(Jets.size()); i++){
    //if(VIS->GetFrame(jetID[i]) == *V) std::cout << "Visible jet pt: " << Jets[i].P.Pt() << ", eta: " << Jets[i].P.Eta() << ", phi: " << Jets[i].P.Phi() << std::endl;}

 
  }
}
