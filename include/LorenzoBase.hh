//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Jan 12 15:42:12 2018 by ROOT version 6.10/04
// from TTree myTree/myTree
// found on file: /afs/cern.ch/user/l/lrossini/public/AthenaCheck/zjet_lowmll/user.lrossini.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.stop4b_v51h_output.root/364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.root
//////////////////////////////////////////////////////////

#ifndef LorenzoBase_h
#define LorenzoBase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

using namespace std;
class LorenzoBase {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   ULong64_t       EventNumber;
   UInt_t          RunNumber;
   Float_t         GenFiltMET;
   Float_t         GenFiltHT;
   UInt_t          LumiBlockNumber;
   Double_t        xsec;
   Float_t         WeightEvents;
   Float_t         WeightEventsPU;
   Float_t         WeightEventselSF;
   Float_t         WeightEventsmuSF;
   Float_t         WeightEvents_trigger_single;
   Float_t         WeightEvents_trigger_dilep;
   Float_t         WeightEvents_trigger_mixlep;
   Float_t         WeightEventsbTag;
   Float_t         WeightEventsJVT;
   Float_t         AverageInteractionsPerCrossing;
   Int_t           NumPrimaryVertices;
   Int_t           DecayType;
   Int_t           HFclass;
   Bool_t          passMETtrig;
   vector<float>   *ptjets_truth;
   vector<float>   *etajets_truth;
   vector<float>   *phijets_truth;
   vector<float>   *massjets_truth;
   vector<int>     *labeljets_truth;
   vector<float>   *mc_eta;
   vector<float>   *mc_pt;
   vector<float>   *mc_mass;
   vector<float>   *mc_phi;
   vector<int>     *mc_pdgid;
   vector<int>     *mc_parent_pdgid;
   Float_t         EtMiss_truthPhi;
   Float_t         EtMiss_truth;
   vector<int>     *labeljets_reco;
   vector<float>   *ptjets;
   vector<float>   *etajets;
   vector<float>   *phijets;
   vector<float>   *massjets;
   vector<bool>    *isB77jets;
   vector<float>   *BtagWeightjets;
   vector<float>   *JVTjets;
   vector<bool>    *passORjet;
   vector<float>   *etaleptons;
   vector<float>   *ptleptons;
   vector<float>   *massleptons;
   vector<float>   *ptvarcone30leptons;
   vector<float>   *ptvarcone20leptons;
   vector<float>   *topoetcone20leptons;
   vector<float>   *phileptons;
   vector<int>     *flavlep;
   vector<int>     *Originlep;
   vector<int>     *Typelep;
   vector<bool>    *passORlep;
   vector<bool>    *isSignallep;
   vector<bool>    *isHighPtlep;
   vector<bool>    *isTightlep;
   vector<bool>    *LepIsoFixedCutLoose;
   vector<bool>    *LepIsoFixedCutTight;
   vector<bool>    *LepIsoFixedCutTightTrackOnly;
   vector<bool>    *LepIsoGradient;
   vector<bool>    *LepIsoGradientLoose;
   vector<bool>    *LepIsoLoose;
   vector<bool>    *LepIsoLooseTrackOnly;
   vector<float>   *d0sig;
   vector<float>   *z0sinTheta;
   Bool_t          cleaningVeto;
   Float_t         EtMiss_tstPhi;
   Float_t         EtMiss_tst;
   Float_t         Etmiss_PVSoftTrkPhi;
   Float_t         Etmiss_PVSoftTrk;
   Int_t           HLT_2e12_lhloose_L12EM10VH;
   Int_t           HLT_2e15_lhvloose_nod0_L12EM13VH;
   Int_t           HLT_2e17_lhvloose_nod0;
   Int_t           HLT_e17_lhloose_mu14;
   Int_t           HLT_e17_lhloose_nod0_mu14;
   Int_t           HLT_mu22_mu8noL1;
   Int_t           HLT_mu20_mu8noL1;
   Int_t           HLT_mu18_mu8noL1;
   Int_t           HLT_e24_lhmedium_L1EM20VH;
   Int_t           HLT_e24_lhmedium_nod0_L1EM20VH;
   Int_t           HLT_e24_lhtight_nod0_ivarloose;
   Int_t           HLT_e26_lhtight_nod0_ivarloose;
   Int_t           HLT_e60_lhmedium;
   Int_t           HLT_e60_lhmedium_nod0;
   Int_t           HLT_e120_lhloose;
   Int_t           HLT_e140_lhloose_nod0;
   Int_t           HLT_mu20_iloose_L1MU15;
   Int_t           HLT_mu24_ivarloose;
   Int_t           HLT_mu24_ivarloose_L1MU15;
   Int_t           HLT_mu24_ivarmedium;
   Int_t           HLT_mu26_ivarmedium;
   Int_t           HLT_mu50;
   Int_t           HLT_xe70_mht;
   Int_t           HLT_xe70_tc_lcw;
   Int_t           HLT_xe80_tc_lcw_L1XE50;
   Int_t           HLT_xe90_mht_L1XE50;
   Int_t           HLT_xe100_mht_L1XE50;
   Int_t           HLT_xe110_mht_L1XE50;
   Int_t           HLT_2mu4_j85_xe50_mht;
   Int_t           HLT_mu4_j125_xe90_mht;

   // List of branches
   TBranch        *b_EventNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_GenFiltMET;   //!
   TBranch        *b_GenFiltHT;   //!
   TBranch        *b_LumiBlock;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_WeightEvents;   //!
   TBranch        *b_WeightEventsPU;   //!
   TBranch        *b_WeightEventselSF;   //!
   TBranch        *b_WeightEventsmuSF;   //!
   TBranch        *b_WeightEvents_trigger_single;   //!
   TBranch        *b_WeightEvents_trigger_dilep;   //!
   TBranch        *b_WeightEvents_trigger_mixlep;   //!
   TBranch        *b_WeightEventsbTag;   //!
   TBranch        *b_WeightEventsJVT;   //!
   TBranch        *b_AverageInteractionsPerCrossing;   //!
   TBranch        *b_NumPrimaryVertices;   //!
   TBranch        *b_DecayType;   //!
   TBranch        *b_HFclass;   //!
   TBranch        *b_passMETtrig;   //!
   TBranch        *b_ptjets_truth;   //!
   TBranch        *b_etajets_truth;   //!
   TBranch        *b_phijets_truth;   //!
   TBranch        *b_massjets_truth;   //!
   TBranch        *b_labeljets_truth;   //!
   TBranch        *b_mc_eta;   //!
   TBranch        *b_mc_pt;   //!
   TBranch        *b_mc_mass;   //!
   TBranch        *b_mc_phi;   //!
   TBranch        *b_mc_pdgid;   //!
   TBranch        *b_mc_parent_pdgid;   //!
   TBranch        *b_EtMiss_truthPhi;   //!
   TBranch        *b_EtMiss_truth;   //!
   TBranch        *b_labeljets_reco;   //!
   TBranch        *b_ptjets;   //!
   TBranch        *b_etajets;   //!
   TBranch        *b_phijets;   //!
   TBranch        *b_massjets;   //!
   TBranch        *b_isB77jets;   //!
   TBranch        *b_BtagWeightjets;   //!
   TBranch        *b_JVTjets;   //!
   TBranch        *b_passORjet;   //!
   TBranch        *b_etaleptons;   //!
   TBranch        *b_ptleptons;   //!
   TBranch        *b_massleptons;   //!
   TBranch        *b_ptvarcone30leptons;   //!
   TBranch        *b_ptvarcone20leptons;   //!
   TBranch        *b_topoetcone20leptons;   //!
   TBranch        *b_phileptons;   //!
   TBranch        *b_flavlep;   //!
   TBranch        *b_Originlep;   //!
   TBranch        *b_Typelep;   //!
   TBranch        *b_passORlep;   //!
   TBranch        *b_isSignallep;   //!
   TBranch        *b_isHighPtlep;   //!
   TBranch        *b_isTightlep;   //!
   TBranch        *b_LepIsoFixedCutLoose;   //!
   TBranch        *b_LepIsoFixedCutTight;   //!
   TBranch        *b_LepIsoFixedCutTightTrackOnly;   //!
   TBranch        *b_LepIsoGradient;   //!
   TBranch        *b_LepIsoGradientLoose;   //!
   TBranch        *b_LepIsoLoose;   //!
   TBranch        *b_LepIsoLooseTrackOnly;   //!
   TBranch        *b_d0sig;   //!
   TBranch        *b_z0sinTheta;   //!
   TBranch        *b_cleaningVeto;   //!
   TBranch        *b_EtMiss_tstPhi;   //!
   TBranch        *b_EtMiss_tst;   //!
   TBranch        *b_Etmiss_PVSoftTrkPhi;   //!
   TBranch        *b_Etmiss_PVSoftTrk;   //!
   TBranch        *b_HLT_2e12_lhloose_L12EM10VH;   //!
   TBranch        *b_HLT_2e15_lhvloose_nod0_L12EM13VH;   //!
   TBranch        *b_HLT_2e17_lhvloose_nod0;   //!
   TBranch        *b_HLT_e17_lhloose_mu14;   //!
   TBranch        *b_HLT_e17_lhloose_nod0_mu14;   //!
   TBranch        *b_HLT_mu22_mu8noL1;   //!
   TBranch        *b_HLT_mu20_mu8noL1;   //!
   TBranch        *b_HLT_mu18_mu8noL1;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e24_lhmedium_nod0_L1EM20VH;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_ivarloose;   //!
   TBranch        *b_HLT_mu24_ivarloose_L1MU15;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_xe70_mht;   //!
   TBranch        *b_HLT_xe70_tc_lcw;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_2mu4_j85_xe50_mht;   //!
   TBranch        *b_HLT_mu4_j125_xe90_mht;   //!

   LorenzoBase(TTree *tree=0);
   virtual ~LorenzoBase();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef LorenzoBase_cxx
LorenzoBase::LorenzoBase(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/afs/cern.ch/user/l/lrossini/public/AthenaCheck/zjet_lowmll/user.lrossini.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.stop4b_v51h_output.root/364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/afs/cern.ch/user/l/lrossini/public/AthenaCheck/zjet_lowmll/user.lrossini.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.stop4b_v51h_output.root/364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.root");
      }
      f->GetObject("myTree",tree);

   }
   Init(tree);
}

LorenzoBase::~LorenzoBase()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t LorenzoBase::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t LorenzoBase::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void LorenzoBase::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   ptjets_truth = 0;
   etajets_truth = 0;
   phijets_truth = 0;
   massjets_truth = 0;
   labeljets_truth = 0;
   mc_eta = 0;
   mc_pt = 0;
   mc_mass = 0;
   mc_phi = 0;
   mc_pdgid = 0;
   mc_parent_pdgid = 0;
   labeljets_reco = 0;
   ptjets = 0;
   etajets = 0;
   phijets = 0;
   massjets = 0;
   isB77jets = 0;
   BtagWeightjets = 0;
   JVTjets = 0;
   passORjet = 0;
   etaleptons = 0;
   ptleptons = 0;
   massleptons = 0;
   ptvarcone30leptons = 0;
   ptvarcone20leptons = 0;
   topoetcone20leptons = 0;
   phileptons = 0;
   flavlep = 0;
   Originlep = 0;
   Typelep = 0;
   passORlep = 0;
   isSignallep = 0;
   isHighPtlep = 0;
   isTightlep = 0;
   LepIsoFixedCutLoose = 0;
   LepIsoFixedCutTight = 0;
   LepIsoFixedCutTightTrackOnly = 0;
   LepIsoGradient = 0;
   LepIsoGradientLoose = 0;
   LepIsoLoose = 0;
   LepIsoLooseTrackOnly = 0;
   d0sig = 0;
   z0sinTheta = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
   fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
   fChain->SetBranchAddress("LumiBlockNumber", &LumiBlockNumber, &b_LumiBlock);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("WeightEvents", &WeightEvents, &b_WeightEvents);
   fChain->SetBranchAddress("WeightEventsPU", &WeightEventsPU, &b_WeightEventsPU);
   fChain->SetBranchAddress("WeightEventselSF", &WeightEventselSF, &b_WeightEventselSF);
   fChain->SetBranchAddress("WeightEventsmuSF", &WeightEventsmuSF, &b_WeightEventsmuSF);
   fChain->SetBranchAddress("WeightEvents_trigger_single", &WeightEvents_trigger_single, &b_WeightEvents_trigger_single);
   fChain->SetBranchAddress("WeightEvents_trigger_dilep", &WeightEvents_trigger_dilep, &b_WeightEvents_trigger_dilep);
   fChain->SetBranchAddress("WeightEvents_trigger_mixlep", &WeightEvents_trigger_mixlep, &b_WeightEvents_trigger_mixlep);
   fChain->SetBranchAddress("WeightEventsbTag", &WeightEventsbTag, &b_WeightEventsbTag);
   fChain->SetBranchAddress("WeightEventsJVT", &WeightEventsJVT, &b_WeightEventsJVT);
   fChain->SetBranchAddress("AverageInteractionsPerCrossing", &AverageInteractionsPerCrossing, &b_AverageInteractionsPerCrossing);
   fChain->SetBranchAddress("NumPrimaryVertices", &NumPrimaryVertices, &b_NumPrimaryVertices);
   fChain->SetBranchAddress("DecayType", &DecayType, &b_DecayType);
   fChain->SetBranchAddress("HFclass", &HFclass, &b_HFclass);
   fChain->SetBranchAddress("passMETtrig", &passMETtrig, &b_passMETtrig);
   fChain->SetBranchAddress("ptjets_truth", &ptjets_truth, &b_ptjets_truth);
   fChain->SetBranchAddress("etajets_truth", &etajets_truth, &b_etajets_truth);
   fChain->SetBranchAddress("phijets_truth", &phijets_truth, &b_phijets_truth);
   fChain->SetBranchAddress("massjets_truth", &massjets_truth, &b_massjets_truth);
   fChain->SetBranchAddress("labeljets_truth", &labeljets_truth, &b_labeljets_truth);
   fChain->SetBranchAddress("mc_eta", &mc_eta, &b_mc_eta);
   fChain->SetBranchAddress("mc_pt", &mc_pt, &b_mc_pt);
   fChain->SetBranchAddress("mc_mass", &mc_mass, &b_mc_mass);
   fChain->SetBranchAddress("mc_phi", &mc_phi, &b_mc_phi);
   fChain->SetBranchAddress("mc_pdgid", &mc_pdgid, &b_mc_pdgid);
   fChain->SetBranchAddress("mc_parent_pdgid", &mc_parent_pdgid, &b_mc_parent_pdgid);
   fChain->SetBranchAddress("EtMiss_truthPhi", &EtMiss_truthPhi, &b_EtMiss_truthPhi);
   fChain->SetBranchAddress("EtMiss_truth", &EtMiss_truth, &b_EtMiss_truth);
   fChain->SetBranchAddress("labeljets_reco", &labeljets_reco, &b_labeljets_reco);
   fChain->SetBranchAddress("ptjets", &ptjets, &b_ptjets);
   fChain->SetBranchAddress("etajets", &etajets, &b_etajets);
   fChain->SetBranchAddress("phijets", &phijets, &b_phijets);
   fChain->SetBranchAddress("massjets", &massjets, &b_massjets);
   fChain->SetBranchAddress("isB77jets", &isB77jets, &b_isB77jets);
   fChain->SetBranchAddress("BtagWeightjets", &BtagWeightjets, &b_BtagWeightjets);
   fChain->SetBranchAddress("JVTjets", &JVTjets, &b_JVTjets);
   fChain->SetBranchAddress("passORjet", &passORjet, &b_passORjet);
   fChain->SetBranchAddress("etaleptons", &etaleptons, &b_etaleptons);
   fChain->SetBranchAddress("ptleptons", &ptleptons, &b_ptleptons);
   fChain->SetBranchAddress("massleptons", &massleptons, &b_massleptons);
   fChain->SetBranchAddress("ptvarcone30leptons", &ptvarcone30leptons, &b_ptvarcone30leptons);
   fChain->SetBranchAddress("ptvarcone20leptons", &ptvarcone20leptons, &b_ptvarcone20leptons);
   fChain->SetBranchAddress("topoetcone20leptons", &topoetcone20leptons, &b_topoetcone20leptons);
   fChain->SetBranchAddress("phileptons", &phileptons, &b_phileptons);
   fChain->SetBranchAddress("flavlep", &flavlep, &b_flavlep);
   fChain->SetBranchAddress("Originlep", &Originlep, &b_Originlep);
   fChain->SetBranchAddress("Typelep", &Typelep, &b_Typelep);
   fChain->SetBranchAddress("passORlep", &passORlep, &b_passORlep);
   fChain->SetBranchAddress("isSignallep", &isSignallep, &b_isSignallep);
   fChain->SetBranchAddress("isHighPtlep", &isHighPtlep, &b_isHighPtlep);
   fChain->SetBranchAddress("isTightlep", &isTightlep, &b_isTightlep);
   fChain->SetBranchAddress("LepIsoFixedCutLoose", &LepIsoFixedCutLoose, &b_LepIsoFixedCutLoose);
   fChain->SetBranchAddress("LepIsoFixedCutTight", &LepIsoFixedCutTight, &b_LepIsoFixedCutTight);
   fChain->SetBranchAddress("LepIsoFixedCutTightTrackOnly", &LepIsoFixedCutTightTrackOnly, &b_LepIsoFixedCutTightTrackOnly);
   fChain->SetBranchAddress("LepIsoGradient", &LepIsoGradient, &b_LepIsoGradient);
   fChain->SetBranchAddress("LepIsoGradientLoose", &LepIsoGradientLoose, &b_LepIsoGradientLoose);
   fChain->SetBranchAddress("LepIsoLoose", &LepIsoLoose, &b_LepIsoLoose);
   fChain->SetBranchAddress("LepIsoLooseTrackOnly", &LepIsoLooseTrackOnly, &b_LepIsoLooseTrackOnly);
   fChain->SetBranchAddress("d0sig", &d0sig, &b_d0sig);
   fChain->SetBranchAddress("z0sinTheta", &z0sinTheta, &b_z0sinTheta);
   fChain->SetBranchAddress("cleaningVeto", &cleaningVeto, &b_cleaningVeto);
   fChain->SetBranchAddress("EtMiss_tstPhi", &EtMiss_tstPhi, &b_EtMiss_tstPhi);
   fChain->SetBranchAddress("EtMiss_tst", &EtMiss_tst, &b_EtMiss_tst);
   fChain->SetBranchAddress("Etmiss_PVSoftTrkPhi", &Etmiss_PVSoftTrkPhi, &b_Etmiss_PVSoftTrkPhi);
   fChain->SetBranchAddress("Etmiss_PVSoftTrk", &Etmiss_PVSoftTrk, &b_Etmiss_PVSoftTrk);
   fChain->SetBranchAddress("HLT_2e12_lhloose_L12EM10VH", &HLT_2e12_lhloose_L12EM10VH, &b_HLT_2e12_lhloose_L12EM10VH);
   fChain->SetBranchAddress("HLT_2e15_lhvloose_nod0_L12EM13VH", &HLT_2e15_lhvloose_nod0_L12EM13VH, &b_HLT_2e15_lhvloose_nod0_L12EM13VH);
   fChain->SetBranchAddress("HLT_2e17_lhvloose_nod0", &HLT_2e17_lhvloose_nod0, &b_HLT_2e17_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_e17_lhloose_mu14", &HLT_e17_lhloose_mu14, &b_HLT_e17_lhloose_mu14);
   fChain->SetBranchAddress("HLT_e17_lhloose_nod0_mu14", &HLT_e17_lhloose_nod0_mu14, &b_HLT_e17_lhloose_nod0_mu14);
   fChain->SetBranchAddress("HLT_mu22_mu8noL1", &HLT_mu22_mu8noL1, &b_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("HLT_mu20_mu8noL1", &HLT_mu20_mu8noL1, &b_HLT_mu20_mu8noL1);
   fChain->SetBranchAddress("HLT_mu18_mu8noL1", &HLT_mu18_mu8noL1, &b_HLT_mu18_mu8noL1);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhmedium_nod0_L1EM20VH", &HLT_e24_lhmedium_nod0_L1EM20VH, &b_HLT_e24_lhmedium_nod0_L1EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("HLT_mu24_ivarloose_L1MU15", &HLT_mu24_ivarloose_L1MU15, &b_HLT_mu24_ivarloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   fChain->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_2mu4_j85_xe50_mht", &HLT_2mu4_j85_xe50_mht, &b_HLT_2mu4_j85_xe50_mht);
   fChain->SetBranchAddress("HLT_mu4_j125_xe90_mht", &HLT_mu4_j125_xe90_mht, &b_HLT_mu4_j125_xe90_mht);
   Notify();
}

Bool_t LorenzoBase::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void LorenzoBase::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t LorenzoBase::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef LorenzoBase_cxx
