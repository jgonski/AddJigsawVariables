//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec  5 23:58:54 2016 by ROOT version 6.04/16
// from TTree data16/
// found on file: dataExample.root
//////////////////////////////////////////////////////////

#ifndef STop1LBase_h
#define STop1LBase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>

// Header file for the classes stored in the TTree if any.

class STop1LBase {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           n_jet;
   Float_t         jet_pt[12];   //[n_jet]
   Float_t         jet_eta[12];   //[n_jet]
   Float_t         jet_phi[12];   //[n_jet]
   Float_t         jet_e[12];   //[n_jet]
   Float_t         jet_mv2c10[12];   //[n_jet]
   Int_t           jet_loosebad[12];   //[n_jet]
   Int_t           jet_tightbad[12];   //[n_jet]
   Float_t         jet_Jvt[12];   //[n_jet]
   Int_t           jet_truthLabel[12];   //[n_jet]
   Int_t           jet_HadronConeExclTruthLabelID[12];   //[n_jet]
   Int_t           jet_killedByPhoton[12];   //[n_jet]
   Int_t           n_bjet;
   Float_t         bjet_pt[4];   //[n_bjet]
   Float_t         bjet_eta[4];   //[n_bjet]
   Float_t         bjet_phi[4];   //[n_bjet]
   Float_t         bjet_e[4];   //[n_bjet]
   Int_t           n_lep;
   Float_t         lep_pt[1];   //[n_lep]
   Float_t         lep_eta[1];   //[n_lep]
   Float_t         lep_phi[1];   //[n_lep]
   Float_t         lep_e[1];   //[n_lep]
   Float_t         lep_charge[1];   //[n_lep]
   Int_t           lep_isoLooseTrackOnly[1];   //[n_lep]
   Int_t           lep_isoLoose[1];   //[n_lep]
   Int_t           lep_isoTight[1];   //[n_lep]
   Int_t           lep_isoGradient[1];   //[n_lep]
   Int_t           lep_isoGradientLoose[1];   //[n_lep]
   Int_t           lep_idMedium[1];   //[n_lep]
   Int_t           lep_idTight[1];   //[n_lep]
   Float_t         lep_ptcone20[1];   //[n_lep]
   Float_t         lep_ptcone30[1];   //[n_lep]
   Float_t         lep_ptcone40[1];   //[n_lep]
   Float_t         lep_ptvarcone20[1];   //[n_lep]
   Float_t         lep_ptvarcone30[1];   //[n_lep]
   Float_t         lep_ptvarcone40[1];   //[n_lep]
   Float_t         lep_topoetcone20[1];   //[n_lep]
   Float_t         lep_topoetcone30[1];   //[n_lep]
   Float_t         lep_topoetcone40[1];   //[n_lep]
   Int_t           n_el;
   Float_t         el_pt[1];   //[n_el]
   Float_t         el_eta[1];   //[n_el]
   Float_t         el_phi[1];   //[n_el]
   Float_t         el_e[1];   //[n_el]
   Float_t         el_charge[1];   //[n_el]
   Int_t           el_isoLooseTrackOnly[1];   //[n_el]
   Int_t           el_isoLoose[1];   //[n_el]
   Int_t           el_isoTight[1];   //[n_el]
   Int_t           el_isoGradient[1];   //[n_el]
   Int_t           el_isoGradientLoose[1];   //[n_el]
   Int_t           el_idMedium[1];   //[n_el]
   Int_t           el_idTight[1];   //[n_el]
   Float_t         el_ptcone20[1];   //[n_el]
   Float_t         el_ptcone30[1];   //[n_el]
   Float_t         el_ptcone40[1];   //[n_el]
   Float_t         el_ptvarcone20[1];   //[n_el]
   Float_t         el_ptvarcone30[1];   //[n_el]
   Float_t         el_ptvarcone40[1];   //[n_el]
   Float_t         el_topoetcone20[1];   //[n_el]
   Float_t         el_topoetcone30[1];   //[n_el]
   Float_t         el_topoetcone40[1];   //[n_el]
   Int_t           n_mu;
   Float_t         mu_pt[1];   //[n_mu]
   Float_t         mu_eta[1];   //[n_mu]
   Float_t         mu_phi[1];   //[n_mu]
   Float_t         mu_e[1];   //[n_mu]
   Float_t         mu_charge[1];   //[n_mu]
   Int_t           mu_cosmic[1];   //[n_mu]
   Int_t           mu_bad[1];   //[n_mu]
   Int_t           mu_isoLooseTrackOnly[1];   //[n_mu]
   Int_t           mu_isoLoose[1];   //[n_mu]
   Int_t           mu_isoTight[1];   //[n_mu]
   Int_t           mu_isoGradient[1];   //[n_mu]
   Int_t           mu_isoGradientLoose[1];   //[n_mu]
   Int_t           mu_idMedium[1];   //[n_mu]
   Int_t           mu_idTight[1];   //[n_mu]
   Int_t           mu_muQuality[1];   //[n_mu]
   Int_t           mu_muHighPt[1];   //[n_mu]
   Float_t         mu_ptcone20[1];   //[n_mu]
   Float_t         mu_ptcone30[1];   //[n_mu]
   Float_t         mu_ptcone40[1];   //[n_mu]
   Float_t         mu_ptvarcone20[1];   //[n_mu]
   Float_t         mu_ptvarcone30[1];   //[n_mu]
   Float_t         mu_ptvarcone40[1];   //[n_mu]
   Float_t         mu_topoetcone20[1];   //[n_mu]
   Float_t         mu_topoetcone30[1];   //[n_mu]
   Float_t         mu_topoetcone40[1];   //[n_mu]
   Int_t           n_ph;
   Float_t         ph_pt[1];   //[n_ph]
   Float_t         ph_eta[1];   //[n_ph]
   Float_t         ph_phi[1];   //[n_ph]
   Float_t         ph_e[1];   //[n_ph]
   Float_t         ph_ptcone20[1];   //[n_ph]
   Float_t         ph_ptcone30[1];   //[n_ph]
   Float_t         ph_topoetcone40[1];   //[n_ph]
   Int_t           ph_isoTight[1];   //[n_ph]
   Int_t           ph_particleTruthOrigin[1];   //[n_ph]
   Int_t           ph_particleTruthType[1];   //[n_ph]
   Int_t           n_tau;
   Float_t         tau_pt[2];   //[n_tau]
   Float_t         tau_eta[2];   //[n_tau]
   Float_t         tau_phi[2];   //[n_tau]
   Float_t         tau_e[2];   //[n_tau]
   Int_t           tau_IsLoose[2];   //[n_tau]
   Int_t           tau_NProng[2];   //[n_tau]
   Float_t         tau_BDTJetScore[2];   //[n_tau]
   Int_t           n_fatjet_R10_L0;
   Float_t         fatjet_R10_L0_pt[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_eta[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_phi[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_e[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_m[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_m_calo[3];   //[n_fatjet_R10_L0]
   Int_t           fatjet_R10_L0_nsmall[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_MaxSmallMass[3];   //[n_fatjet_R10_L0]
   Float_t         fatjet_R10_L0_abs_dphi_met[3];   //[n_fatjet_R10_L0]
   Int_t           n_fatjet_mo_R10_L0;
   Float_t         fatjet_mo_R10_L0_pt[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_eta[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_phi[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_e[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_m[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_m_calo[3];   //[n_fatjet_mo_R10_L0]
   Int_t           fatjet_mo_R10_L0_pt_index[3];   //[n_fatjet_mo_R10_L0]
   Int_t           fatjet_mo_R10_L0_nsmall[3];   //[n_fatjet_mo_R10_L0]
   Float_t         fatjet_mo_R10_L0_abs_dphi_met[3];   //[n_fatjet_mo_R10_L0]
   Int_t           n_fatjet_R12_L0;
   Float_t         fatjet_R12_L0_pt[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_eta[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_phi[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_e[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_m[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_m_calo[4];   //[n_fatjet_R12_L0]
   Int_t           fatjet_R12_L0_nsmall[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_MaxSmallMass[4];   //[n_fatjet_R12_L0]
   Float_t         fatjet_R12_L0_abs_dphi_met[4];   //[n_fatjet_R12_L0]
   Int_t           n_fatjet_mo_R12_L0;
   Float_t         fatjet_mo_R12_L0_pt[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_eta[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_phi[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_e[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_m[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_m_calo[4];   //[n_fatjet_mo_R12_L0]
   Int_t           fatjet_mo_R12_L0_pt_index[4];   //[n_fatjet_mo_R12_L0]
   Int_t           fatjet_mo_R12_L0_nsmall[4];   //[n_fatjet_mo_R12_L0]
   Float_t         fatjet_mo_R12_L0_abs_dphi_met[4];   //[n_fatjet_mo_R12_L0]
   Int_t           n_hadtop;
   Float_t         hadtop_pt[1];   //[n_hadtop]
   Float_t         hadtop_eta[1];   //[n_hadtop]
   Float_t         hadtop_phi[1];   //[n_hadtop]
   Float_t         hadtop_e[1];   //[n_hadtop]
   Int_t           hadtop_bjet_index[1];   //[n_hadtop]
   Int_t           hadtop_light_jet1_index[1];   //[n_hadtop]
   Int_t           hadtop_light_jet2_index[1];   //[n_hadtop]
   Float_t         hadtop_chi2[1];   //[n_hadtop]
   Float_t         hadtop_m[1];   //[n_hadtop]
   Int_t           n_leptop;
   Float_t         leptop_pt[1];   //[n_leptop]
   Float_t         leptop_eta[1];   //[n_leptop]
   Float_t         leptop_phi[1];   //[n_leptop]
   Float_t         leptop_e[1];   //[n_leptop]
   Int_t           leptop_bjet_index[1];   //[n_leptop]
   Int_t           n_truth_matched_jet;
   Float_t         truth_matched_jet_pt[1];   //[n_truth_matched_jet]
   Float_t         truth_matched_jet_eta[1];   //[n_truth_matched_jet]
   Float_t         truth_matched_jet_phi[1];   //[n_truth_matched_jet]
   Float_t         truth_matched_jet_e[1];   //[n_truth_matched_jet]
   Int_t           truth_matched_jet_n_matches[1];   //[n_truth_matched_jet]
   Int_t           n_rrtop;
   Float_t         rrtop_pt[3];   //[n_rrtop]
   Float_t         rrtop_eta[3];   //[n_rrtop]
   Float_t         rrtop_phi[3];   //[n_rrtop]
   Float_t         rrtop_e[3];   //[n_rrtop]
   Float_t         rrtop_m[3];   //[n_rrtop]
   Float_t         rrtop_m_calo[3];   //[n_rrtop]
   Float_t         rrtop_r[3];   //[n_rrtop]
   Int_t           rrtop_n_constit[3];   //[n_rrtop]
   Int_t           rrtop_n_btag[3];   //[n_rrtop]
   Int_t           rrtop_top_match[3];   //[n_rrtop]
   Int_t           rrtop_cand_match[3];   //[n_rrtop]
   Int_t           rrtop_jets_match[3];   //[n_rrtop]
   Int_t           rrtop_bjet_match[3];   //[n_rrtop]
   Int_t           rrtop_ljet1_match[3];   //[n_rrtop]
   Int_t           rrtop_ljet2_match[3];   //[n_rrtop]
   Float_t         rrtop_h0_pt[3];   //[n_rrtop]
   Float_t         rrtop_h0_r[3];   //[n_rrtop]
   Int_t           rrtop_h0_n[3];   //[n_rrtop]
   Int_t           rrtop_h0_match[3];   //[n_rrtop]
   Int_t           rrtop_h0_status[3];   //[n_rrtop]
   Float_t         rrtop_h1_pt[3];   //[n_rrtop]
   Float_t         rrtop_h1_r[3];   //[n_rrtop]
   Int_t           rrtop_h1_n[3];   //[n_rrtop]
   Int_t           rrtop_h1_match[3];   //[n_rrtop]
   Int_t           rrtop_h1_status[3];   //[n_rrtop]
   Float_t         rrtop_h2_pt[3];   //[n_rrtop]
   Float_t         rrtop_h2_r[3];   //[n_rrtop]
   Int_t           rrtop_h2_n[3];   //[n_rrtop]
   Int_t           rrtop_h2_match[3];   //[n_rrtop]
   Int_t           rrtop_h2_status[3];   //[n_rrtop]
   Int_t           n_bad_rrtop;
   Float_t         bad_rrtop_pt[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_eta[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_phi[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_e[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_m[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_r[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_n_constit[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_n_btag[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_top_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_cand_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_jets_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_bjet_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_ljet1_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_ljet2_match[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h0_pt[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h0_r[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h0_n[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h0_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h0_status[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h1_pt[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h1_r[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h1_n[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h1_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h1_status[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h2_pt[4];   //[n_bad_rrtop]
   Float_t         bad_rrtop_h2_r[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h2_n[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h2_match[4];   //[n_bad_rrtop]
   Int_t           bad_rrtop_h2_status[4];   //[n_bad_rrtop]
   Int_t           n_hadtop_cand;
   Float_t         hadtop_cand_pt[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_eta[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_phi[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_e[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_m[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_m_calo[2];   //[n_hadtop_cand]
   Float_t         hadtop_cand_r[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_n_constit[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_n_btag[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_top_match[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_cand_match[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_jets_match[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_bjet_match[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_ljet1_match[2];   //[n_hadtop_cand]
   Int_t           hadtop_cand_ljet2_match[2];   //[n_hadtop_cand]
   Int_t           n_truth_photon;
   Float_t         truth_photon_pt[1];   //[n_truth_photon]
   Float_t         truth_photon_eta[1];   //[n_truth_photon]
   Float_t         truth_photon_phi[1];   //[n_truth_photon]
   Float_t         truth_photon_e[1];   //[n_truth_photon]
   Int_t           truth_photon_motherPdgId[1];   //[n_truth_photon]
   Int_t           truth_photon_statusCode[1];   //[n_truth_photon]
   Int_t           truth_photon_barCode[1];   //[n_truth_photon]
   Int_t           n_truth_top;
   Float_t         truth_top_pt[1];   //[n_truth_top]
   Float_t         truth_top_eta[1];   //[n_truth_top]
   Float_t         truth_top_phi[1];   //[n_truth_top]
   Float_t         truth_top_e[1];   //[n_truth_top]
   Float_t         met;
   Float_t         met_x;
   Float_t         met_y;
   Float_t         met_phi;
   Float_t         met_sumet;
   Float_t         met_el;
   Float_t         met_el_x;
   Float_t         met_el_y;
   Float_t         met_el_phi;
   Float_t         met_el_sumet;
   Float_t         met_mu;
   Float_t         met_mu_x;
   Float_t         met_mu_y;
   Float_t         met_mu_phi;
   Float_t         met_mu_sumet;
   Float_t         met_jet;
   Float_t         met_jet_x;
   Float_t         met_jet_y;
   Float_t         met_jet_phi;
   Float_t         met_jet_sumet;
   Float_t         met_soft;
   Float_t         met_soft_x;
   Float_t         met_soft_y;
   Float_t         met_soft_phi;
   Float_t         met_soft_sumet;
   Float_t         met_cst;
   Float_t         met_cst_x;
   Float_t         met_cst_y;
   Float_t         met_cst_phi;
   Float_t         met_cst_sumet;
   Float_t         met_cst_soft;
   Float_t         met_photons;
   Float_t         met_photons_x;
   Float_t         met_photons_y;
   Float_t         met_photons_phi;
   Float_t         met_photons_sumet;
   Float_t         mc_event_weight;
   Float_t         weight;
   Float_t         sf_btag;
   Float_t         sf_jvt;
   Float_t         sf_el;
   Float_t         sf_mu;
   Float_t         sf_ph;
   Float_t         sf_el_trigger;
   Float_t         sf_mu_trigger;
   Float_t         sf_total;
   Float_t         sf_no_pu;
   Float_t         pileup_weight;
   Float_t         sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         sf_total__SYST_FT_EFF_B_systematics__1down;
   Float_t         sf_no_pu__SYST_FT_EFF_B_systematics__1down;
   Float_t         sf_total__SYST_FT_EFF_B_systematics__1up;
   Float_t         sf_no_pu__SYST_FT_EFF_B_systematics__1up;
   Float_t         sf_total__SYST_FT_EFF_C_systematics__1down;
   Float_t         sf_no_pu__SYST_FT_EFF_C_systematics__1down;
   Float_t         sf_total__SYST_FT_EFF_C_systematics__1up;
   Float_t         sf_no_pu__SYST_FT_EFF_C_systematics__1up;
   Float_t         sf_total__SYST_FT_EFF_Light_systematics__1down;
   Float_t         sf_no_pu__SYST_FT_EFF_Light_systematics__1down;
   Float_t         sf_total__SYST_FT_EFF_Light_systematics__1up;
   Float_t         sf_no_pu__SYST_FT_EFF_Light_systematics__1up;
   Float_t         sf_total__SYST_FT_EFF_extrapolation__1down;
   Float_t         sf_no_pu__SYST_FT_EFF_extrapolation__1down;
   Float_t         sf_total__SYST_FT_EFF_extrapolation__1up;
   Float_t         sf_no_pu__SYST_FT_EFF_extrapolation__1up;
   Float_t         sf_total__SYST_FT_EFF_extrapolation_from_charm__1down;
   Float_t         sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1down;
   Float_t         sf_total__SYST_FT_EFF_extrapolation_from_charm__1up;
   Float_t         sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1up;
   Float_t         sf_total__SYST_JET_JvtEfficiency__1down;
   Float_t         sf_no_pu__SYST_JET_JvtEfficiency__1down;
   Float_t         sf_total__SYST_JET_JvtEfficiency__1up;
   Float_t         sf_no_pu__SYST_JET_JvtEfficiency__1up;
   Float_t         sf_total__SYST_MUON_EFF_STAT__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_STAT__1down;
   Float_t         sf_total__SYST_MUON_EFF_STAT__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_STAT__1up;
   Float_t         sf_total__SYST_MUON_EFF_STAT_LOWPT__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1down;
   Float_t         sf_total__SYST_MUON_EFF_STAT_LOWPT__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1up;
   Float_t         sf_total__SYST_MUON_EFF_SYS__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_SYS__1down;
   Float_t         sf_total__SYST_MUON_EFF_SYS__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_SYS__1up;
   Float_t         sf_total__SYST_MUON_EFF_SYS_LOWPT__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1down;
   Float_t         sf_total__SYST_MUON_EFF_SYS_LOWPT__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1up;
   Float_t         sf_total__SYST_MUON_EFF_TrigStatUncertainty__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1down;
   Float_t         sf_total__SYST_MUON_EFF_TrigStatUncertainty__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1up;
   Float_t         sf_total__SYST_MUON_EFF_TrigSystUncertainty__1down;
   Float_t         sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1down;
   Float_t         sf_total__SYST_MUON_EFF_TrigSystUncertainty__1up;
   Float_t         sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1up;
   Float_t         sf_total__SYST_MUON_ISO_STAT__1down;
   Float_t         sf_no_pu__SYST_MUON_ISO_STAT__1down;
   Float_t         sf_total__SYST_MUON_ISO_STAT__1up;
   Float_t         sf_no_pu__SYST_MUON_ISO_STAT__1up;
   Float_t         sf_total__SYST_MUON_ISO_SYS__1down;
   Float_t         sf_no_pu__SYST_MUON_ISO_SYS__1down;
   Float_t         sf_total__SYST_MUON_ISO_SYS__1up;
   Float_t         sf_no_pu__SYST_MUON_ISO_SYS__1up;
   Float_t         sf_total__SYST_MUON_TTVA_STAT__1down;
   Float_t         sf_no_pu__SYST_MUON_TTVA_STAT__1down;
   Float_t         sf_total__SYST_MUON_TTVA_STAT__1up;
   Float_t         sf_no_pu__SYST_MUON_TTVA_STAT__1up;
   Float_t         sf_total__SYST_MUON_TTVA_SYS__1down;
   Float_t         sf_no_pu__SYST_MUON_TTVA_SYS__1down;
   Float_t         sf_total__SYST_MUON_TTVA_SYS__1up;
   Float_t         sf_no_pu__SYST_MUON_TTVA_SYS__1up;
   Float_t         sf_total__SYST_PRW_DATASF__1down;
   Float_t         sf_no_pu__SYST_PRW_DATASF__1down;
   Float_t         sf_total__SYST_PRW_DATASF__1up;
   Float_t         sf_no_pu__SYST_PRW_DATASF__1up;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
   Float_t         sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
   Float_t         sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
   Float_t         av_int_per_xing;
   Float_t         num_pv;
   Float_t         neutrino_met;
   Float_t         gen_filter_met;
   Float_t         weight_stop_reweighting_right;
   Float_t         weight_stop_reweighting_left;
   Float_t         weight_bC_reweighting_rightright;
   Float_t         weight_bC_reweighting_leftleft;
   Float_t         weight_bC_reweighting_rightleft;
   Float_t         weight_bC_reweighting_leftright;
   Float_t         weight_sherpa22_njets;
   Float_t         met_truth;
   Float_t         met_dphi;
   Float_t         dphi_met_fatjet0_R10_L0;
   Float_t         dphi_met_fatjet1_R10_L0;
   Float_t         dphi_met_fatjet0_R12_L0;
   Float_t         dphi_met_fatjet1_R12_L0;
   Float_t         mt;
   Float_t         blinding;
   Float_t         dr_qq;
   Float_t         min_dr_bq;
   Float_t         max_dr_bq;
   Float_t         truth_hadtop_pt;
   Float_t         truth_hadtop_eta;
   Float_t         truth_hadtop_phi;
   Float_t         truth_hadtop_e;
   Float_t         truth_hadtop_b_pt;
   Float_t         truth_hadtop_b_eta;
   Float_t         truth_hadtop_b_phi;
   Float_t         truth_hadtop_b_e;
   Float_t         truth_hadtop_q1_pt;
   Float_t         truth_hadtop_q1_eta;
   Float_t         truth_hadtop_q1_phi;
   Float_t         truth_hadtop_q1_e;
   Float_t         truth_hadtop_q2_pt;
   Float_t         truth_hadtop_q2_eta;
   Float_t         truth_hadtop_q2_phi;
   Float_t         truth_hadtop_q2_e;
   Float_t         dphi_jet0_ptmiss;
   Float_t         dphi_jet1_ptmiss;
   Float_t         dphi_jet2_ptmiss;
   Float_t         dphi_jet3_ptmiss;
   Float_t         dphi_min_ptmiss;
   Float_t         dphi_met_lep;
   Float_t         dphi_b_lep_min;
   Float_t         dphi_b_lep_max;
   Float_t         dphi_b_ptmiss_min;
   Float_t         dphi_b_ptmiss_max;
   Float_t         met_proj_lep;
   Float_t         lepPt_over_met;
   Float_t         lepPt_over_met_lep;
   Float_t         ht;
   Float_t         met_sig;
   Float_t         m_top;
   Float_t         m_top_chi2;
   Float_t         m_top_chi2_chi2;
   Float_t         dr_bjet_lep;
   Float_t         mindr_bjet_lep;
   Float_t         top_dr_bjet_lep;
   Float_t         amt2;
   Float_t         mt2_tau;
   Float_t         mt2stop;
   Float_t         mT2tauLooseTau_GeV;
   Float_t         topness;
   Float_t         photon_mt;
   Float_t         photon_met;
   Float_t         ht_sig;
   Float_t         ht_sig_num;
   Float_t         ht_sig_denom;
   Float_t         ht_sig_sumet;
   Float_t         ht_sig_dphi;
   Float_t         ht_sig_dphi_reco;
   Float_t         ht_sig_photon;
   Float_t         deltaRbb_ordermv2;
   Float_t         deltaRbb_orderpt;
   Float_t         m_bl;
   Float_t         m_b1l;
   Float_t         m_b2l;
   Float_t         m_bj;
   Float_t         m_b1j;
   Float_t         m_b2j;
   Float_t         mT_blMET;
   Float_t         mT_b1lMET;
   Float_t         mT_b2lMET;
   Float_t         sumPt_b1b2;
   Float_t         m_bb;
   Float_t         m_bl_max;
   Float_t         m_bl_min;
   Float_t         mT_blMET_max;
   Float_t         mT_blMET_min;
   Float_t         nnlo_weight;
   Float_t         nnlo_weight_topptup;
   Float_t         nnlo_weight_ttbarptup;
   Float_t         nnlo_weight_exttoppt;
   Float_t         met_perp;
   Float_t         ttbar_m;
   Float_t         ttbar_pt;
   Float_t         dphi_ttbar;
   Float_t         dphi_leptop_met;
   Float_t         dphi_hadtop_met;
   Float_t         weight_top_mass_reweighting;
   Float_t         BDT;
   Int_t           treatAsYear;
   Int_t           random_run_number;
   Int_t           is_mc;
   Int_t           is_TT_signal;
   Int_t           has_extra_met;
   Int_t           TT_decay_mode;
   Int_t           tt_cat;
   Int_t           el_trigger;
   Int_t           mu_trigger;
   Int_t           xe_trigger;
   Int_t           stxe_trigger;
   Int_t           ph_trigger;
   Int_t           dilep_trigger;
   Int_t           HLT_e24_lhmedium_L1EM20VH;
   Int_t           HLT_e26_lhtight_nod0_ivarloose;
   Int_t           HLT_e60_lhmedium;
   Int_t           HLT_e60_lhmedium_nod0;
   Int_t           HLT_e120_lhloose;
   Int_t           HLT_e140_lhloose_nod0;
   Int_t           HLT_mu20_iloose_L1MU15;
   Int_t           HLT_mu26_ivarmedium;
   Int_t           HLT_mu40;
   Int_t           HLT_mu50;
   Int_t           HLT_xe70_mht;
   Int_t           HLT_xe80_tc_lcw_L1XE50;
   Int_t           HLT_xe90_mht_L1XE50;
   Int_t           HLT_xe100_mht_L1XE50;
   Int_t           HLT_xe110_mht_L1XE50;
   Int_t           HLT_g120_loose;
   Int_t           HLT_g140_loose;
   Int_t           HLT_e17_lhloose_mu14;
   Int_t           HLT_e24_lhmedium_L1EM20VHI_mu8noL1;
   Int_t           HLT_e7_lhmedium_mu24;
   Int_t           HLT_e17_lhloose_nod0_mu14;
   Int_t           HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1;
   Int_t           HLT_e7_lhmedium_nod0_mu24;
   Int_t           HLT_e12_lhloose_nod0_2mu10;
   Int_t           HLT_2e12_lhloose_nod0_mu10;
   Int_t           EventPassesLooseJetCleaning;
   Int_t           EventPassesTightJetCleaning;
   Int_t           JetWithHotCaloCells;
   Int_t           n_cosmics;
   Int_t           has_hadtop;
   Int_t           has_bjet;
   Int_t           has_ljet1;
   Int_t           has_ljet2;
   Int_t           rr_hadtop_good_cand;
   Int_t           rr_hadtop_good_jet;
   Int_t           truth_hasMEphoton;
   Int_t           passMEphoton;
   Int_t           SRpresel;
   Int_t           SR1;
   Int_t           CR1presel;
   Int_t           TCR1;
   Int_t           WCR1;
   Int_t           STCR1;
   Int_t           TVR1;
   Int_t           WVR1;
   Int_t           TZCR1;
   Int_t           ttZCRpresel;
   Int_t           tN_med;
   Int_t           CR_tN_med_presel;
   Int_t           CR_tN_med_topVeto;
   Int_t           T1LCR_tN_med;
   Int_t           T2LCR_tN_med;
   Int_t           WCR_tN_med;
   Int_t           STCR_tN_med;
   Int_t           TVR_tN_med;
   Int_t           WVR_tN_med;
   Int_t           TZCR_tN_med;
   Int_t           tN_high;
   Int_t           CR_tN_high_presel;
   Int_t           CR_tN_high_topVeto;
   Int_t           T1LCR_tN_high;
   Int_t           T2LCR_tN_high;
   Int_t           WCR_tN_high;
   Int_t           STCR_tN_high;
   Int_t           TVR_tN_high;
   Int_t           WVR_tN_high;
   Int_t           TZCR_tN_high;
   Int_t           tN_high_ICHEP;
   Int_t           CR_tN_high_ICHEP_presel;
   Int_t           TCR_tN_high_ICHEP;
   Int_t           WCR_tN_high_ICHEP;
   Int_t           STCR_tN_high_ICHEP;
   Int_t           TVR_tN_high_ICHEP;
   Int_t           WVR_tN_high_ICHEP;
   Int_t           TZCR_tN_high_ICHEP;
   Int_t           vlq_low;
   Int_t           vlq_high;
   Int_t           vlq_combined;
   Int_t           cr_vlq_presel;
   Int_t           TCR_vlq;
   Int_t           WCR_vlq;
   Int_t           TVR_vlq;
   Int_t           WVR_vlq;
   Int_t           bC2x_med;
   Int_t           bC2x_med_TCR;
   Int_t           bC2x_med_STCR;
   Int_t           bC2x_med_WCR;
   Int_t           bC2x_med_WVR;
   Int_t           bC2x_med_TVR;
   Int_t           bC2x_med_TZCR;
   Int_t           bC2x_diag;
   Int_t           bC2x_diag_TCR;
   Int_t           bC2x_diag_STCR;
   Int_t           bC2x_diag_WCR;
   Int_t           bC2x_diag_TVR;
   Int_t           bC2x_diag_WVR;
   Int_t           bC2x_diag_TZCR;
   Int_t           bCbv;
   Int_t           bCbv_TCR;
   Int_t           bCbv_WCR;
   Int_t           bCbv_TVR;
   Int_t           bCbv_WVR;
   Int_t           bCbv_SRsideband;
   Int_t           DM_high;
   Int_t           CR_DM_high_presel;
   Int_t           TCR_DM_high;
   Int_t           WCR_DM_high;
   Int_t           STCR_DM_high;
   Int_t           TVR_DM_high;
   Int_t           WVR_DM_high;
   Int_t           TZCR_DM_high;
   Int_t           DM_low;
   Int_t           CR_DM_low_presel;
   Int_t           TCR_DM_low;
   Int_t           WCR_DM_low;
   Int_t           STCR_DM_low;
   Int_t           TVR_DM_low;
   Int_t           WVR_DM_low;
   Int_t           TZCR_DM_low;
   Int_t           WtailVR;
   Int_t           DileptonVR;
   Int_t           LeptonTauVR;
   Int_t           amt2tailVR;
   Int_t           amt2dilresVR;
   Int_t           amt2dilboostVR;
   UInt_t          run_number;
   UInt_t          lumi_block;
   UInt_t          mc_channel_number;
   UInt_t          bcid;
   UInt_t          SRpresel_n1;
   UInt_t          SR1_n1;
   UInt_t          CR1presel_n1;
   UInt_t          TCR1_n1;
   UInt_t          WCR1_n1;
   UInt_t          STCR1_n1;
   UInt_t          TVR1_n1;
   UInt_t          WVR1_n1;
   UInt_t          TZCR1_n1;
   UInt_t          ttZCRpresel_n1;
   UInt_t          tN_med_n1;
   UInt_t          CR_tN_med_presel_n1;
   UInt_t          CR_tN_med_topVeto_n1;
   UInt_t          T1LCR_tN_med_n1;
   UInt_t          T2LCR_tN_med_n1;
   UInt_t          WCR_tN_med_n1;
   UInt_t          STCR_tN_med_n1;
   UInt_t          TVR_tN_med_n1;
   UInt_t          WVR_tN_med_n1;
   UInt_t          TZCR_tN_med_n1;
   UInt_t          tN_high_n1;
   UInt_t          CR_tN_high_presel_n1;
   UInt_t          CR_tN_high_topVeto_n1;
   UInt_t          T1LCR_tN_high_n1;
   UInt_t          T2LCR_tN_high_n1;
   UInt_t          WCR_tN_high_n1;
   UInt_t          STCR_tN_high_n1;
   UInt_t          TVR_tN_high_n1;
   UInt_t          WVR_tN_high_n1;
   UInt_t          TZCR_tN_high_n1;
   UInt_t          tN_high_ICHEP_n1;
   UInt_t          CR_tN_high_ICHEP_presel_n1;
   UInt_t          TCR_tN_high_ICHEP_n1;
   UInt_t          WCR_tN_high_ICHEP_n1;
   UInt_t          STCR_tN_high_ICHEP_n1;
   UInt_t          TVR_tN_high_ICHEP_n1;
   UInt_t          WVR_tN_high_ICHEP_n1;
   UInt_t          TZCR_tN_high_ICHEP_n1;
   UInt_t          vlq_low_n1;
   UInt_t          vlq_high_n1;
   UInt_t          vlq_combined_n1;
   UInt_t          cr_vlq_presel_n1;
   UInt_t          TCR_vlq_n1;
   UInt_t          WCR_vlq_n1;
   UInt_t          TVR_vlq_n1;
   UInt_t          WVR_vlq_n1;
   UInt_t          bC2x_med_n1;
   UInt_t          bC2x_med_TCR_n1;
   UInt_t          bC2x_med_STCR_n1;
   UInt_t          bC2x_med_WCR_n1;
   UInt_t          bC2x_med_WVR_n1;
   UInt_t          bC2x_med_TVR_n1;
   UInt_t          bC2x_med_TZCR_n1;
   UInt_t          bC2x_diag_n1;
   UInt_t          bC2x_diag_TCR_n1;
   UInt_t          bC2x_diag_STCR_n1;
   UInt_t          bC2x_diag_WCR_n1;
   UInt_t          bC2x_diag_TVR_n1;
   UInt_t          bC2x_diag_WVR_n1;
   UInt_t          bC2x_diag_TZCR_n1;
   UInt_t          bCbv_n1;
   UInt_t          bCbv_TCR_n1;
   UInt_t          bCbv_WCR_n1;
   UInt_t          bCbv_TVR_n1;
   UInt_t          bCbv_WVR_n1;
   UInt_t          bCbv_SRsideband_n1;
   UInt_t          DM_high_n1;
   UInt_t          CR_DM_high_presel_n1;
   UInt_t          TCR_DM_high_n1;
   UInt_t          WCR_DM_high_n1;
   UInt_t          STCR_DM_high_n1;
   UInt_t          TVR_DM_high_n1;
   UInt_t          WVR_DM_high_n1;
   UInt_t          TZCR_DM_high_n1;
   UInt_t          DM_low_n1;
   UInt_t          CR_DM_low_presel_n1;
   UInt_t          TCR_DM_low_n1;
   UInt_t          WCR_DM_low_n1;
   UInt_t          STCR_DM_low_n1;
   UInt_t          TVR_DM_low_n1;
   UInt_t          WVR_DM_low_n1;
   UInt_t          TZCR_DM_low_n1;
   UInt_t          WtailVR_n1;
   UInt_t          DileptonVR_n1;
   UInt_t          LeptonTauVR_n1;
   UInt_t          amt2tailVR_n1;
   UInt_t          amt2dilresVR_n1;
   UInt_t          amt2dilboostVR_n1;
   ULong64_t       event_number;
   ULong64_t       prw_hash;
   ULong64_t       prw_hash_tt;
   Float_t         xs_weight;

   // List of branches
   TBranch        *b_n_jet;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_mv2c10;   //!
   TBranch        *b_jet_loosebad;   //!
   TBranch        *b_jet_tightbad;   //!
   TBranch        *b_jet_Jvt;   //!
   TBranch        *b_jet_truthLabel;   //!
   TBranch        *b_jet_HadronConeExclTruthLabelID;   //!
   TBranch        *b_jet_killedByPhoton;   //!
   TBranch        *b_n_bjet;   //!
   TBranch        *b_bjet_pt;   //!
   TBranch        *b_bjet_eta;   //!
   TBranch        *b_bjet_phi;   //!
   TBranch        *b_bjet_e;   //!
   TBranch        *b_n_lep;   //!
   TBranch        *b_lep_pt;   //!
   TBranch        *b_lep_eta;   //!
   TBranch        *b_lep_phi;   //!
   TBranch        *b_lep_e;   //!
   TBranch        *b_lep_isoLooseTrackOnly;   //!
   TBranch        *b_lep_isoLoose;   //!
   TBranch        *b_lep_isoTight;   //!
   TBranch        *b_lep_isoGradient;   //!
   TBranch        *b_lep_isoGradientLoose;   //!
   TBranch        *b_lep_idMedium;   //!
   TBranch        *b_lep_idTight;   //!
   TBranch        *b_lep_ptcone20;   //!
   TBranch        *b_lep_ptcone30;   //!
   TBranch        *b_lep_ptcone40;   //!
   TBranch        *b_lep_ptvarcone20;   //!
   TBranch        *b_lep_ptvarcone30;   //!
   TBranch        *b_lep_ptvarcone40;   //!
   TBranch        *b_lep_topoetcone20;   //!
   TBranch        *b_lep_topoetcone30;   //!
   TBranch        *b_lep_topoetcone40;   //!
   TBranch        *b_n_el;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_isoLooseTrackOnly;   //!
   TBranch        *b_el_isoLoose;   //!
   TBranch        *b_el_isoTight;   //!
   TBranch        *b_el_isoGradient;   //!
   TBranch        *b_el_isoGradientLoose;   //!
   TBranch        *b_el_idMedium;   //!
   TBranch        *b_el_idTight;   //!
   TBranch        *b_el_ptcone20;   //!
   TBranch        *b_el_ptcone30;   //!
   TBranch        *b_el_ptcone40;   //!
   TBranch        *b_el_ptvarcone20;   //!
   TBranch        *b_el_ptvarcone30;   //!
   TBranch        *b_el_ptvarcone40;   //!
   TBranch        *b_el_topoetcone20;   //!
   TBranch        *b_el_topoetcone30;   //!
   TBranch        *b_el_topoetcone40;   //!
   TBranch        *b_n_mu;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_mu_cosmic;   //!
   TBranch        *b_mu_bad;   //!
   TBranch        *b_mu_isoLooseTrackOnly;   //!
   TBranch        *b_mu_isoLoose;   //!
   TBranch        *b_mu_isoTight;   //!
   TBranch        *b_mu_isoGradient;   //!
   TBranch        *b_mu_isoGradientLoose;   //!
   TBranch        *b_mu_idMedium;   //!
   TBranch        *b_mu_idTight;   //!
   TBranch        *b_mu_muQuality;   //!
   TBranch        *b_mu_muHighPt;   //!
   TBranch        *b_mu_ptcone20;   //!
   TBranch        *b_mu_ptcone30;   //!
   TBranch        *b_mu_ptcone40;   //!
   TBranch        *b_mu_ptvarcone20;   //!
   TBranch        *b_mu_ptvarcone30;   //!
   TBranch        *b_mu_ptvarcone40;   //!
   TBranch        *b_mu_topoetcone20;   //!
   TBranch        *b_mu_topoetcone30;   //!
   TBranch        *b_mu_topoetcone40;   //!
   TBranch        *b_n_ph;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_e;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_ptcone30;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_isoTight;   //!
   TBranch        *b_ph_particleTruthOrigin;   //!
   TBranch        *b_ph_particleTruthType;   //!
   TBranch        *b_n_tau;   //!
   TBranch        *b_tau_pt;   //!
   TBranch        *b_tau_eta;   //!
   TBranch        *b_tau_phi;   //!
   TBranch        *b_tau_e;   //!
   TBranch        *b_tau_IsLoose;   //!
   TBranch        *b_tau_NProng;   //!
   TBranch        *b_tau_BDTJetScore;   //!
   TBranch        *b_n_fatjet_R10_L0;   //!
   TBranch        *b_fatjet_R10_L0_pt;   //!
   TBranch        *b_fatjet_R10_L0_eta;   //!
   TBranch        *b_fatjet_R10_L0_phi;   //!
   TBranch        *b_fatjet_R10_L0_e;   //!
   TBranch        *b_fatjet_R10_L0_m;   //!
   TBranch        *b_fatjet_R10_L0_m_calo;   //!
   TBranch        *b_fatjet_R10_L0_nsmall;   //!
   TBranch        *b_fatjet_R10_L0_MaxSmallMass;   //!
   TBranch        *b_fatjet_R10_L0_abs_dphi_met;   //!
   TBranch        *b_n_fatjet_mo_R10_L0;   //!
   TBranch        *b_fatjet_mo_R10_L0_pt;   //!
   TBranch        *b_fatjet_mo_R10_L0_eta;   //!
   TBranch        *b_fatjet_mo_R10_L0_phi;   //!
   TBranch        *b_fatjet_mo_R10_L0_e;   //!
   TBranch        *b_fatjet_mo_R10_L0_m;   //!
   TBranch        *b_fatjet_mo_R10_L0_m_calo;   //!
   TBranch        *b_fatjet_mo_R10_L0_pt_index;   //!
   TBranch        *b_fatjet_mo_R10_L0_nsmall;   //!
   TBranch        *b_fatjet_mo_R10_L0_abs_dphi_met;   //!
   TBranch        *b_n_fatjet_R12_L0;   //!
   TBranch        *b_fatjet_R12_L0_pt;   //!
   TBranch        *b_fatjet_R12_L0_eta;   //!
   TBranch        *b_fatjet_R12_L0_phi;   //!
   TBranch        *b_fatjet_R12_L0_e;   //!
   TBranch        *b_fatjet_R12_L0_m;   //!
   TBranch        *b_fatjet_R12_L0_m_calo;   //!
   TBranch        *b_fatjet_R12_L0_nsmall;   //!
   TBranch        *b_fatjet_R12_L0_MaxSmallMass;   //!
   TBranch        *b_fatjet_R12_L0_abs_dphi_met;   //!
   TBranch        *b_n_fatjet_mo_R12_L0;   //!
   TBranch        *b_fatjet_mo_R12_L0_pt;   //!
   TBranch        *b_fatjet_mo_R12_L0_eta;   //!
   TBranch        *b_fatjet_mo_R12_L0_phi;   //!
   TBranch        *b_fatjet_mo_R12_L0_e;   //!
   TBranch        *b_fatjet_mo_R12_L0_m;   //!
   TBranch        *b_fatjet_mo_R12_L0_m_calo;   //!
   TBranch        *b_fatjet_mo_R12_L0_pt_index;   //!
   TBranch        *b_fatjet_mo_R12_L0_nsmall;   //!
   TBranch        *b_fatjet_mo_R12_L0_abs_dphi_met;   //!
   TBranch        *b_n_hadtop;   //!
   TBranch        *b_hadtop_pt;   //!
   TBranch        *b_hadtop_eta;   //!
   TBranch        *b_hadtop_phi;   //!
   TBranch        *b_hadtop_e;   //!
   TBranch        *b_hadtop_bjet_index;   //!
   TBranch        *b_hadtop_light_jet1_index;   //!
   TBranch        *b_hadtop_light_jet2_index;   //!
   TBranch        *b_hadtop_chi2;   //!
   TBranch        *b_hadtop_m;   //!
   TBranch        *b_n_leptop;   //!
   TBranch        *b_leptop_pt;   //!
   TBranch        *b_leptop_eta;   //!
   TBranch        *b_leptop_phi;   //!
   TBranch        *b_leptop_e;   //!
   TBranch        *b_leptop_bjet_index;   //!
   TBranch        *b_n_truth_matched_jet;   //!
   TBranch        *b_truth_matched_jet_pt;   //!
   TBranch        *b_truth_matched_jet_eta;   //!
   TBranch        *b_truth_matched_jet_phi;   //!
   TBranch        *b_truth_matched_jet_e;   //!
   TBranch        *b_truth_matched_jet_n_matches;   //!
   TBranch        *b_n_rrtop;   //!
   TBranch        *b_rrtop_pt;   //!
   TBranch        *b_rrtop_eta;   //!
   TBranch        *b_rrtop_phi;   //!
   TBranch        *b_rrtop_e;   //!
   TBranch        *b_rrtop_m;   //!
   TBranch        *b_rrtop_m_calo;   //!
   TBranch        *b_rrtop_r;   //!
   TBranch        *b_rrtop_n_constit;   //!
   TBranch        *b_rrtop_n_btag;   //!
   TBranch        *b_rrtop_top_match;   //!
   TBranch        *b_rrtop_cand_match;   //!
   TBranch        *b_rrtop_jets_match;   //!
   TBranch        *b_rrtop_bjet_match;   //!
   TBranch        *b_rrtop_ljet1_match;   //!
   TBranch        *b_rrtop_ljet2_match;   //!
   TBranch        *b_rrtop_h0_pt;   //!
   TBranch        *b_rrtop_h0_r;   //!
   TBranch        *b_rrtop_h0_n;   //!
   TBranch        *b_rrtop_h0_match;   //!
   TBranch        *b_rrtop_h0_status;   //!
   TBranch        *b_rrtop_h1_pt;   //!
   TBranch        *b_rrtop_h1_r;   //!
   TBranch        *b_rrtop_h1_n;   //!
   TBranch        *b_rrtop_h1_match;   //!
   TBranch        *b_rrtop_h1_status;   //!
   TBranch        *b_rrtop_h2_pt;   //!
   TBranch        *b_rrtop_h2_r;   //!
   TBranch        *b_rrtop_h2_n;   //!
   TBranch        *b_rrtop_h2_match;   //!
   TBranch        *b_rrtop_h2_status;   //!
   TBranch        *b_n_bad_rrtop;   //!
   TBranch        *b_bad_rrtop_pt;   //!
   TBranch        *b_bad_rrtop_eta;   //!
   TBranch        *b_bad_rrtop_phi;   //!
   TBranch        *b_bad_rrtop_e;   //!
   TBranch        *b_bad_rrtop_m;   //!
   TBranch        *b_bad_rrtop_r;   //!
   TBranch        *b_bad_rrtop_n_constit;   //!
   TBranch        *b_bad_rrtop_n_btag;   //!
   TBranch        *b_bad_rrtop_top_match;   //!
   TBranch        *b_bad_rrtop_cand_match;   //!
   TBranch        *b_bad_rrtop_jets_match;   //!
   TBranch        *b_bad_rrtop_bjet_match;   //!
   TBranch        *b_bad_rrtop_ljet1_match;   //!
   TBranch        *b_bad_rrtop_ljet2_match;   //!
   TBranch        *b_bad_rrtop_h0_pt;   //!
   TBranch        *b_bad_rrtop_h0_r;   //!
   TBranch        *b_bad_rrtop_h0_n;   //!
   TBranch        *b_bad_rrtop_h0_match;   //!
   TBranch        *b_bad_rrtop_h0_status;   //!
   TBranch        *b_bad_rrtop_h1_pt;   //!
   TBranch        *b_bad_rrtop_h1_r;   //!
   TBranch        *b_bad_rrtop_h1_n;   //!
   TBranch        *b_bad_rrtop_h1_match;   //!
   TBranch        *b_bad_rrtop_h1_status;   //!
   TBranch        *b_bad_rrtop_h2_pt;   //!
   TBranch        *b_bad_rrtop_h2_r;   //!
   TBranch        *b_bad_rrtop_h2_n;   //!
   TBranch        *b_bad_rrtop_h2_match;   //!
   TBranch        *b_bad_rrtop_h2_status;   //!
   TBranch        *b_n_hadtop_cand;   //!
   TBranch        *b_hadtop_cand_pt;   //!
   TBranch        *b_hadtop_cand_eta;   //!
   TBranch        *b_hadtop_cand_phi;   //!
   TBranch        *b_hadtop_cand_e;   //!
   TBranch        *b_hadtop_cand_m;   //!
   TBranch        *b_hadtop_cand_m_calo;   //!
   TBranch        *b_hadtop_cand_r;   //!
   TBranch        *b_hadtop_cand_n_constit;   //!
   TBranch        *b_hadtop_cand_n_btag;   //!
   TBranch        *b_hadtop_cand_top_match;   //!
   TBranch        *b_hadtop_cand_cand_match;   //!
   TBranch        *b_hadtop_cand_jets_match;   //!
   TBranch        *b_hadtop_cand_bjet_match;   //!
   TBranch        *b_hadtop_cand_ljet1_match;   //!
   TBranch        *b_hadtop_cand_ljet2_match;   //!
   TBranch        *b_n_truth_photon;   //!
   TBranch        *b_truth_photon_pt;   //!
   TBranch        *b_truth_photon_eta;   //!
   TBranch        *b_truth_photon_phi;   //!
   TBranch        *b_truth_photon_e;   //!
   TBranch        *b_truth_photon_motherPdgId;   //!
   TBranch        *b_truth_photon_statusCode;   //!
   TBranch        *b_truth_photon_barCode;   //!
   TBranch        *b_n_truth_top;   //!
   TBranch        *b_truth_top_pt;   //!
   TBranch        *b_truth_top_eta;   //!
   TBranch        *b_truth_top_phi;   //!
   TBranch        *b_truth_top_e;   //!
   TBranch        *b_met;   //!
   TBranch        *b_met_x;   //!
   TBranch        *b_met_y;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_met_sumet;   //!
   TBranch        *b_met_el;   //!
   TBranch        *b_met_el_x;   //!
   TBranch        *b_met_el_y;   //!
   TBranch        *b_met_el_phi;   //!
   TBranch        *b_met_el_sumet;   //!
   TBranch        *b_met_mu;   //!
   TBranch        *b_met_mu_x;   //!
   TBranch        *b_met_mu_y;   //!
   TBranch        *b_met_mu_phi;   //!
   TBranch        *b_met_mu_sumet;   //!
   TBranch        *b_met_jet;   //!
   TBranch        *b_met_jet_x;   //!
   TBranch        *b_met_jet_y;   //!
   TBranch        *b_met_jet_phi;   //!
   TBranch        *b_met_jet_sumet;   //!
   TBranch        *b_met_soft;   //!
   TBranch        *b_met_soft_x;   //!
   TBranch        *b_met_soft_y;   //!
   TBranch        *b_met_soft_phi;   //!
   TBranch        *b_met_soft_sumet;   //!
   TBranch        *b_met_cst;   //!
   TBranch        *b_met_cst_x;   //!
   TBranch        *b_met_cst_y;   //!
   TBranch        *b_met_cst_phi;   //!
   TBranch        *b_met_cst_sumet;   //!
   TBranch        *b_met_cst_soft;   //!
   TBranch        *b_met_photons;   //!
   TBranch        *b_met_photons_x;   //!
   TBranch        *b_met_photons_y;   //!
   TBranch        *b_met_photons_phi;   //!
   TBranch        *b_met_photons_sumet;   //!
   TBranch        *b_mc_event_weight;   //!
   TBranch        *b_weight;   //!
   TBranch        *b_sf_btag;   //!
   TBranch        *b_sf_jvt;   //!
   TBranch        *b_sf_el;   //!
   TBranch        *b_sf_mu;   //!
   TBranch        *b_sf_ph;   //!
   TBranch        *b_sf_el_trigger;   //!
   TBranch        *b_sf_mu_trigger;   //!
   TBranch        *b_sf_total;   //!
   TBranch        *b_sf_no_pu;   //!
   TBranch        *b_pileup_weight;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_sf_total__SYST_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_sf_total__SYST_JET_JvtEfficiency__1down;   //!
   TBranch        *b_sf_no_pu__SYST_JET_JvtEfficiency__1down;   //!
   TBranch        *b_sf_total__SYST_JET_JvtEfficiency__1up;   //!
   TBranch        *b_sf_no_pu__SYST_JET_JvtEfficiency__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_STAT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_STAT__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_STAT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_STAT__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_STAT_LOWPT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_STAT_LOWPT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_SYS__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_SYS__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_SYS__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_SYS__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_SYS_LOWPT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_SYS_LOWPT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_ISO_STAT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_ISO_STAT__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_ISO_STAT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_ISO_STAT__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_ISO_SYS__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_ISO_SYS__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_ISO_SYS__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_ISO_SYS__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_TTVA_STAT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_TTVA_STAT__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_TTVA_STAT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_TTVA_STAT__1up;   //!
   TBranch        *b_sf_total__SYST_MUON_TTVA_SYS__1down;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_TTVA_SYS__1down;   //!
   TBranch        *b_sf_total__SYST_MUON_TTVA_SYS__1up;   //!
   TBranch        *b_sf_no_pu__SYST_MUON_TTVA_SYS__1up;   //!
   TBranch        *b_sf_total__SYST_PRW_DATASF__1down;   //!
   TBranch        *b_sf_no_pu__SYST_PRW_DATASF__1down;   //!
   TBranch        *b_sf_total__SYST_PRW_DATASF__1up;   //!
   TBranch        *b_sf_no_pu__SYST_PRW_DATASF__1up;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;   //!
   TBranch        *b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;   //!
   TBranch        *b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;   //!
   TBranch        *b_av_int_per_xing;   //!
   TBranch        *b_num_pv;   //!
   TBranch        *b_neutrino_met;   //!
   TBranch        *b_gen_filter_met;   //!
   TBranch        *b_weight_stop_reweighting_right;   //!
   TBranch        *b_weight_stop_reweighting_left;   //!
   TBranch        *b_weight_bC_reweighting_rightright;   //!
   TBranch        *b_weight_bC_reweighting_leftleft;   //!
   TBranch        *b_weight_bC_reweighting_rightleft;   //!
   TBranch        *b_weight_bC_reweighting_leftright;   //!
   TBranch        *b_weight_sherpa22_njets;   //!
   TBranch        *b_met_truth;   //!
   TBranch        *b_met_dphi;   //!
   TBranch        *b_dphi_met_fatjet0_R10_L0;   //!
   TBranch        *b_dphi_met_fatjet1_R10_L0;   //!
   TBranch        *b_dphi_met_fatjet0_R12_L0;   //!
   TBranch        *b_dphi_met_fatjet1_R12_L0;   //!
   TBranch        *b_mt;   //!
   TBranch        *b_blinding;   //!
   TBranch        *b_dr_qq;   //!
   TBranch        *b_min_dr_bq;   //!
   TBranch        *b_max_dr_bq;   //!
   TBranch        *b_truth_hadtop_pt;   //!
   TBranch        *b_truth_hadtop_eta;   //!
   TBranch        *b_truth_hadtop_phi;   //!
   TBranch        *b_truth_hadtop_e;   //!
   TBranch        *b_truth_hadtop_b_pt;   //!
   TBranch        *b_truth_hadtop_b_eta;   //!
   TBranch        *b_truth_hadtop_b_phi;   //!
   TBranch        *b_truth_hadtop_b_e;   //!
   TBranch        *b_truth_hadtop_q1_pt;   //!
   TBranch        *b_truth_hadtop_q1_eta;   //!
   TBranch        *b_truth_hadtop_q1_phi;   //!
   TBranch        *b_truth_hadtop_q1_e;   //!
   TBranch        *b_truth_hadtop_q2_pt;   //!
   TBranch        *b_truth_hadtop_q2_eta;   //!
   TBranch        *b_truth_hadtop_q2_phi;   //!
   TBranch        *b_truth_hadtop_q2_e;   //!
   TBranch        *b_dphi_jet0_ptmiss;   //!
   TBranch        *b_dphi_jet1_ptmiss;   //!
   TBranch        *b_dphi_jet2_ptmiss;   //!
   TBranch        *b_dphi_jet3_ptmiss;   //!
   TBranch        *b_dphi_min_ptmiss;   //!
   TBranch        *b_dphi_met_lep;   //!
   TBranch        *b_dphi_b_lep_min;   //!
   TBranch        *b_dphi_b_lep_max;   //!
   TBranch        *b_dphi_b_ptmiss_min;   //!
   TBranch        *b_dphi_b_ptmiss_max;   //!
   TBranch        *b_met_proj_lep;   //!
   TBranch        *b_lepPt_over_met;   //!
   TBranch        *b_lepPt_over_met_lep;   //!
   TBranch        *b_ht;   //!
   TBranch        *b_met_sig;   //!
   TBranch        *b_m_top;   //!
   TBranch        *b_m_top_chi2;   //!
   TBranch        *b_m_top_chi2_chi2;   //!
   TBranch        *b_dr_bjet_lep;   //!
   TBranch        *b_mindr_bjet_lep;   //!
   TBranch        *b_top_dr_bjet_lep;   //!
   TBranch        *b_amt2;   //!
   TBranch        *b_mt2_tau;   //!
   TBranch        *b_mt2stop;   //!
   TBranch        *b_mT2tauLooseTau_GeV;   //!
   TBranch        *b_topness;   //!
   TBranch        *b_photon_mt;   //!
   TBranch        *b_photon_met;   //!
   TBranch        *b_ht_sig;   //!
   TBranch        *b_ht_sig_num;   //!
   TBranch        *b_ht_sig_denom;   //!
   TBranch        *b_ht_sig_sumet;   //!
   TBranch        *b_ht_sig_dphi;   //!
   TBranch        *b_ht_sig_dphi_reco;   //!
   TBranch        *b_ht_sig_photon;   //!
   TBranch        *b_deltaRbb_ordermv2;   //!
   TBranch        *b_deltaRbb_orderpt;   //!
   TBranch        *b_m_bl;   //!
   TBranch        *b_m_b1l;   //!
   TBranch        *b_m_b2l;   //!
   TBranch        *b_m_bj;   //!
   TBranch        *b_m_b1j;   //!
   TBranch        *b_m_b2j;   //!
   TBranch        *b_mT_blMET;   //!
   TBranch        *b_mT_b1lMET;   //!
   TBranch        *b_mT_b2lMET;   //!
   TBranch        *b_sumPt_b1b2;   //!
   TBranch        *b_m_bb;   //!
   TBranch        *b_m_bl_max;   //!
   TBranch        *b_m_bl_min;   //!
   TBranch        *b_mT_blMET_max;   //!
   TBranch        *b_mT_blMET_min;   //!
   TBranch        *b_nnlo_weight;   //!
   TBranch        *b_nnlo_weight_topptup;   //!
   TBranch        *b_nnlo_weight_ttbarptup;   //!
   TBranch        *b_nnlo_weight_exttoppt;   //!
   TBranch        *b_met_perp;   //!
   TBranch        *b_ttbar_m;   //!
   TBranch        *b_ttbar_pt;   //!
   TBranch        *b_dphi_ttbar;   //!
   TBranch        *b_dphi_leptop_met;   //!
   TBranch        *b_dphi_hadtop_met;   //!
   TBranch        *b_weight_top_mass_reweighting;   //!
   TBranch        *b_BDT;   //!
   TBranch        *b_treatAsYear;   //!
   TBranch        *b_random_run_number;   //!
   TBranch        *b_is_mc;   //!
   TBranch        *b_is_TT_signal;   //!
   TBranch        *b_has_extra_met;   //!
   TBranch        *b_TT_decay_mode;   //!
   TBranch        *b_tt_cat;   //!
   TBranch        *b_el_trigger;   //!
   TBranch        *b_mu_trigger;   //!
   TBranch        *b_xe_trigger;   //!
   TBranch        *b_stxe_trigger;   //!
   TBranch        *b_ph_trigger;   //!
   TBranch        *b_dilep_trigger;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_xe70_mht;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_g120_loose;   //!
   TBranch        *b_HLT_g140_loose;   //!
   TBranch        *b_HLT_e17_lhloose_mu14;   //!
   TBranch        *b_HLT_e24_lhmedium_L1EM20VHI_mu8noL1;   //!
   TBranch        *b_HLT_e7_lhmedium_mu24;   //!
   TBranch        *b_HLT_e17_lhloose_nod0_mu14;   //!
   TBranch        *b_HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1;   //!
   TBranch        *b_HLT_e7_lhmedium_nod0_mu24;   //!
   TBranch        *b_HLT_e12_lhloose_nod0_2mu10;   //!
   TBranch        *b_HLT_2e12_lhloose_nod0_mu10;   //!
   TBranch        *b_EventPassesLooseJetCleaning;   //!
   TBranch        *b_EventPassesTightJetCleaning;   //!
   TBranch        *b_JetWithHotCaloCells;   //!
   TBranch        *b_n_cosmics;   //!
   TBranch        *b_has_hadtop;   //!
   TBranch        *b_has_bjet;   //!
   TBranch        *b_has_ljet1;   //!
   TBranch        *b_has_ljet2;   //!
   TBranch        *b_rr_hadtop_good_cand;   //!
   TBranch        *b_rr_hadtop_good_jet;   //!
   TBranch        *b_truth_hasMEphoton;   //!
   TBranch        *b_passMEphoton;   //!
   TBranch        *b_SRpresel;   //!
   TBranch        *b_SR1;   //!
   TBranch        *b_CR1presel;   //!
   TBranch        *b_TCR1;   //!
   TBranch        *b_WCR1;   //!
   TBranch        *b_STCR1;   //!
   TBranch        *b_TVR1;   //!
   TBranch        *b_WVR1;   //!
   TBranch        *b_TZCR1;   //!
   TBranch        *b_ttZCRpresel;   //!
   TBranch        *b_tN_med;   //!
   TBranch        *b_CR_tN_med_presel;   //!
   TBranch        *b_CR_tN_med_topVeto;   //!
   TBranch        *b_T1LCR_tN_med;   //!
   TBranch        *b_T2LCR_tN_med;   //!
   TBranch        *b_WCR_tN_med;   //!
   TBranch        *b_STCR_tN_med;   //!
   TBranch        *b_TVR_tN_med;   //!
   TBranch        *b_WVR_tN_med;   //!
   TBranch        *b_TZCR_tN_med;   //!
   TBranch        *b_tN_high;   //!
   TBranch        *b_CR_tN_high_presel;   //!
   TBranch        *b_CR_tN_high_topVeto;   //!
   TBranch        *b_T1LCR_tN_high;   //!
   TBranch        *b_T2LCR_tN_high;   //!
   TBranch        *b_WCR_tN_high;   //!
   TBranch        *b_STCR_tN_high;   //!
   TBranch        *b_TVR_tN_high;   //!
   TBranch        *b_WVR_tN_high;   //!
   TBranch        *b_TZCR_tN_high;   //!
   TBranch        *b_tN_high_ICHEP;   //!
   TBranch        *b_CR_tN_high_ICHEP_presel;   //!
   TBranch        *b_TCR_tN_high_ICHEP;   //!
   TBranch        *b_WCR_tN_high_ICHEP;   //!
   TBranch        *b_STCR_tN_high_ICHEP;   //!
   TBranch        *b_TVR_tN_high_ICHEP;   //!
   TBranch        *b_WVR_tN_high_ICHEP;   //!
   TBranch        *b_TZCR_tN_high_ICHEP;   //!
   TBranch        *b_vlq_low;   //!
   TBranch        *b_vlq_high;   //!
   TBranch        *b_vlq_combined;   //!
   TBranch        *b_cr_vlq_presel;   //!
   TBranch        *b_TCR_vlq;   //!
   TBranch        *b_WCR_vlq;   //!
   TBranch        *b_TVR_vlq;   //!
   TBranch        *b_WVR_vlq;   //!
   TBranch        *b_bC2x_med;   //!
   TBranch        *b_bC2x_med_TCR;   //!
   TBranch        *b_bC2x_med_STCR;   //!
   TBranch        *b_bC2x_med_WCR;   //!
   TBranch        *b_bC2x_med_WVR;   //!
   TBranch        *b_bC2x_med_TVR;   //!
   TBranch        *b_bC2x_med_TZCR;   //!
   TBranch        *b_bC2x_diag;   //!
   TBranch        *b_bC2x_diag_TCR;   //!
   TBranch        *b_bC2x_diag_STCR;   //!
   TBranch        *b_bC2x_diag_WCR;   //!
   TBranch        *b_bC2x_diag_TVR;   //!
   TBranch        *b_bC2x_diag_WVR;   //!
   TBranch        *b_bC2x_diag_TZCR;   //!
   TBranch        *b_bCbv;   //!
   TBranch        *b_bCbv_TCR;   //!
   TBranch        *b_bCbv_WCR;   //!
   TBranch        *b_bCbv_TVR;   //!
   TBranch        *b_bCbv_WVR;   //!
   TBranch        *b_bCbv_SRsideband;   //!
   TBranch        *b_DM_high;   //!
   TBranch        *b_CR_DM_high_presel;   //!
   TBranch        *b_TCR_DM_high;   //!
   TBranch        *b_WCR_DM_high;   //!
   TBranch        *b_STCR_DM_high;   //!
   TBranch        *b_TVR_DM_high;   //!
   TBranch        *b_WVR_DM_high;   //!
   TBranch        *b_TZCR_DM_high;   //!
   TBranch        *b_DM_low;   //!
   TBranch        *b_CR_DM_low_presel;   //!
   TBranch        *b_TCR_DM_low;   //!
   TBranch        *b_WCR_DM_low;   //!
   TBranch        *b_STCR_DM_low;   //!
   TBranch        *b_TVR_DM_low;   //!
   TBranch        *b_WVR_DM_low;   //!
   TBranch        *b_TZCR_DM_low;   //!
   TBranch        *b_WtailVR;   //!
   TBranch        *b_DileptonVR;   //!
   TBranch        *b_LeptonTauVR;   //!
   TBranch        *b_amt2tailVR;   //!
   TBranch        *b_amt2dilresVR;   //!
   TBranch        *b_amt2dilboostVR;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_lumi_block;   //!
   TBranch        *b_mc_channel_number;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_SRpresel_n1;   //!
   TBranch        *b_SR1_n1;   //!
   TBranch        *b_CR1presel_n1;   //!
   TBranch        *b_TCR1_n1;   //!
   TBranch        *b_WCR1_n1;   //!
   TBranch        *b_STCR1_n1;   //!
   TBranch        *b_TVR1_n1;   //!
   TBranch        *b_WVR1_n1;   //!
   TBranch        *b_TZCR1_n1;   //!
   TBranch        *b_ttZCRpresel_n1;   //!
   TBranch        *b_tN_med_n1;   //!
   TBranch        *b_CR_tN_med_presel_n1;   //!
   TBranch        *b_CR_tN_med_topVeto_n1;   //!
   TBranch        *b_T1LCR_tN_med_n1;   //!
   TBranch        *b_T2LCR_tN_med_n1;   //!
   TBranch        *b_WCR_tN_med_n1;   //!
   TBranch        *b_STCR_tN_med_n1;   //!
   TBranch        *b_TVR_tN_med_n1;   //!
   TBranch        *b_WVR_tN_med_n1;   //!
   TBranch        *b_TZCR_tN_med_n1;   //!
   TBranch        *b_tN_high_n1;   //!
   TBranch        *b_CR_tN_high_presel_n1;   //!
   TBranch        *b_CR_tN_high_topVeto_n1;   //!
   TBranch        *b_T1LCR_tN_high_n1;   //!
   TBranch        *b_T2LCR_tN_high_n1;   //!
   TBranch        *b_WCR_tN_high_n1;   //!
   TBranch        *b_STCR_tN_high_n1;   //!
   TBranch        *b_TVR_tN_high_n1;   //!
   TBranch        *b_WVR_tN_high_n1;   //!
   TBranch        *b_TZCR_tN_high_n1;   //!
   TBranch        *b_tN_high_ICHEP_n1;   //!
   TBranch        *b_CR_tN_high_ICHEP_presel_n1;   //!
   TBranch        *b_TCR_tN_high_ICHEP_n1;   //!
   TBranch        *b_WCR_tN_high_ICHEP_n1;   //!
   TBranch        *b_STCR_tN_high_ICHEP_n1;   //!
   TBranch        *b_TVR_tN_high_ICHEP_n1;   //!
   TBranch        *b_WVR_tN_high_ICHEP_n1;   //!
   TBranch        *b_TZCR_tN_high_ICHEP_n1;   //!
   TBranch        *b_vlq_low_n1;   //!
   TBranch        *b_vlq_high_n1;   //!
   TBranch        *b_vlq_combined_n1;   //!
   TBranch        *b_cr_vlq_presel_n1;   //!
   TBranch        *b_TCR_vlq_n1;   //!
   TBranch        *b_WCR_vlq_n1;   //!
   TBranch        *b_TVR_vlq_n1;   //!
   TBranch        *b_WVR_vlq_n1;   //!
   TBranch        *b_bC2x_med_n1;   //!
   TBranch        *b_bC2x_med_TCR_n1;   //!
   TBranch        *b_bC2x_med_STCR_n1;   //!
   TBranch        *b_bC2x_med_WCR_n1;   //!
   TBranch        *b_bC2x_med_WVR_n1;   //!
   TBranch        *b_bC2x_med_TVR_n1;   //!
   TBranch        *b_bC2x_med_TZCR_n1;   //!
   TBranch        *b_bC2x_diag_n1;   //!
   TBranch        *b_bC2x_diag_TCR_n1;   //!
   TBranch        *b_bC2x_diag_STCR_n1;   //!
   TBranch        *b_bC2x_diag_WCR_n1;   //!
   TBranch        *b_bC2x_diag_TVR_n1;   //!
   TBranch        *b_bC2x_diag_WVR_n1;   //!
   TBranch        *b_bC2x_diag_TZCR_n1;   //!
   TBranch        *b_bCbv_n1;   //!
   TBranch        *b_bCbv_TCR_n1;   //!
   TBranch        *b_bCbv_WCR_n1;   //!
   TBranch        *b_bCbv_TVR_n1;   //!
   TBranch        *b_bCbv_WVR_n1;   //!
   TBranch        *b_bCbv_SRsideband_n1;   //!
   TBranch        *b_DM_high_n1;   //!
   TBranch        *b_CR_DM_high_presel_n1;   //!
   TBranch        *b_TCR_DM_high_n1;   //!
   TBranch        *b_WCR_DM_high_n1;   //!
   TBranch        *b_STCR_DM_high_n1;   //!
   TBranch        *b_TVR_DM_high_n1;   //!
   TBranch        *b_WVR_DM_high_n1;   //!
   TBranch        *b_TZCR_DM_high_n1;   //!
   TBranch        *b_DM_low_n1;   //!
   TBranch        *b_CR_DM_low_presel_n1;   //!
   TBranch        *b_TCR_DM_low_n1;   //!
   TBranch        *b_WCR_DM_low_n1;   //!
   TBranch        *b_STCR_DM_low_n1;   //!
   TBranch        *b_TVR_DM_low_n1;   //!
   TBranch        *b_WVR_DM_low_n1;   //!
   TBranch        *b_TZCR_DM_low_n1;   //!
   TBranch        *b_WtailVR_n1;   //!
   TBranch        *b_DileptonVR_n1;   //!
   TBranch        *b_LeptonTauVR_n1;   //!
   TBranch        *b_amt2tailVR_n1;   //!
   TBranch        *b_amt2dilresVR_n1;   //!
   TBranch        *b_amt2dilboostVR_n1;   //!
   TBranch        *b_event_number;   //!
   TBranch        *b_prw_hash;   //!
   TBranch        *b_prw_hash_tt;   //!
   TBranch        *b_xs_weight;   //!

   STop1LBase(TTree *tree=0);
   virtual ~STop1LBase();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
  //virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef STop1LBase_cxx
STop1LBase::STop1LBase(TTree *tree) : fChain(0) 
{
   if (tree == 0) {
     /*TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("dataExample.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("dataExample.root");
      }
      f->GetObject("data16",tree);*/
     std::cout << "ERROR - Input ROOT file not found and/or can't load TTree.. :-(" << std::endl;
   }
   Init(tree);
}

STop1LBase::~STop1LBase()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t STop1LBase::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t STop1LBase::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void STop1LBase::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("jet_pt", jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_mv2c10", jet_mv2c10, &b_jet_mv2c10);
   fChain->SetBranchAddress("jet_loosebad", jet_loosebad, &b_jet_loosebad);
   fChain->SetBranchAddress("jet_tightbad", jet_tightbad, &b_jet_tightbad);
   fChain->SetBranchAddress("jet_Jvt", jet_Jvt, &b_jet_Jvt);
   fChain->SetBranchAddress("jet_truthLabel", jet_truthLabel, &b_jet_truthLabel);
   fChain->SetBranchAddress("jet_HadronConeExclTruthLabelID", jet_HadronConeExclTruthLabelID, &b_jet_HadronConeExclTruthLabelID);
   fChain->SetBranchAddress("jet_killedByPhoton", jet_killedByPhoton, &b_jet_killedByPhoton);
   fChain->SetBranchAddress("n_bjet", &n_bjet, &b_n_bjet);
   fChain->SetBranchAddress("bjet_pt", bjet_pt, &b_bjet_pt);
   fChain->SetBranchAddress("bjet_eta", bjet_eta, &b_bjet_eta);
   fChain->SetBranchAddress("bjet_phi", bjet_phi, &b_bjet_phi);
   fChain->SetBranchAddress("bjet_e", bjet_e, &b_bjet_e);
   fChain->SetBranchAddress("n_lep", &n_lep, &b_n_lep);
   fChain->SetBranchAddress("lep_pt", lep_pt, &b_lep_pt);
   fChain->SetBranchAddress("lep_eta", lep_eta, &b_lep_eta);
   fChain->SetBranchAddress("lep_phi", lep_phi, &b_lep_phi);
   fChain->SetBranchAddress("lep_e", lep_e, &b_lep_e);
   fChain->SetBranchAddress("lep_isoLooseTrackOnly", lep_isoLooseTrackOnly, &b_lep_isoLooseTrackOnly);
   fChain->SetBranchAddress("lep_isoLoose", lep_isoLoose, &b_lep_isoLoose);
   fChain->SetBranchAddress("lep_isoTight", lep_isoTight, &b_lep_isoTight);
   fChain->SetBranchAddress("lep_isoGradient", lep_isoGradient, &b_lep_isoGradient);
   fChain->SetBranchAddress("lep_isoGradientLoose", lep_isoGradientLoose, &b_lep_isoGradientLoose);
   fChain->SetBranchAddress("lep_idMedium", lep_idMedium, &b_lep_idMedium);
   fChain->SetBranchAddress("lep_idTight", lep_idTight, &b_lep_idTight);
   fChain->SetBranchAddress("lep_ptcone20", lep_ptcone20, &b_lep_ptcone20);
   fChain->SetBranchAddress("lep_ptcone30", lep_ptcone30, &b_lep_ptcone30);
   fChain->SetBranchAddress("lep_ptcone40", lep_ptcone40, &b_lep_ptcone40);
   fChain->SetBranchAddress("lep_ptvarcone20", lep_ptvarcone20, &b_lep_ptvarcone20);
   fChain->SetBranchAddress("lep_ptvarcone30", lep_ptvarcone30, &b_lep_ptvarcone30);
   fChain->SetBranchAddress("lep_ptvarcone40", lep_ptvarcone40, &b_lep_ptvarcone40);
   fChain->SetBranchAddress("lep_topoetcone20", lep_topoetcone20, &b_lep_topoetcone20);
   fChain->SetBranchAddress("lep_topoetcone30", lep_topoetcone30, &b_lep_topoetcone30);
   fChain->SetBranchAddress("lep_topoetcone40", lep_topoetcone40, &b_lep_topoetcone40);
   fChain->SetBranchAddress("n_el", &n_el, &b_n_el);
   fChain->SetBranchAddress("el_pt", el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_isoLooseTrackOnly", el_isoLooseTrackOnly, &b_el_isoLooseTrackOnly);
   fChain->SetBranchAddress("el_isoLoose", el_isoLoose, &b_el_isoLoose);
   fChain->SetBranchAddress("el_isoTight", el_isoTight, &b_el_isoTight);
   fChain->SetBranchAddress("el_isoGradient", el_isoGradient, &b_el_isoGradient);
   fChain->SetBranchAddress("el_isoGradientLoose", el_isoGradientLoose, &b_el_isoGradientLoose);
   fChain->SetBranchAddress("el_idMedium", el_idMedium, &b_el_idMedium);
   fChain->SetBranchAddress("el_idTight", el_idTight, &b_el_idTight);
   fChain->SetBranchAddress("el_ptcone20", el_ptcone20, &b_el_ptcone20);
   fChain->SetBranchAddress("el_ptcone30", el_ptcone30, &b_el_ptcone30);
   fChain->SetBranchAddress("el_ptcone40", el_ptcone40, &b_el_ptcone40);
   fChain->SetBranchAddress("el_ptvarcone20", el_ptvarcone20, &b_el_ptvarcone20);
   fChain->SetBranchAddress("el_ptvarcone30", el_ptvarcone30, &b_el_ptvarcone30);
   fChain->SetBranchAddress("el_ptvarcone40", el_ptvarcone40, &b_el_ptvarcone40);
   fChain->SetBranchAddress("el_topoetcone20", el_topoetcone20, &b_el_topoetcone20);
   fChain->SetBranchAddress("el_topoetcone30", el_topoetcone30, &b_el_topoetcone30);
   fChain->SetBranchAddress("el_topoetcone40", el_topoetcone40, &b_el_topoetcone40);
   fChain->SetBranchAddress("n_mu", &n_mu, &b_n_mu);
   fChain->SetBranchAddress("mu_pt", mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("mu_cosmic", mu_cosmic, &b_mu_cosmic);
   fChain->SetBranchAddress("mu_bad", mu_bad, &b_mu_bad);
   fChain->SetBranchAddress("mu_isoLooseTrackOnly", mu_isoLooseTrackOnly, &b_mu_isoLooseTrackOnly);
   fChain->SetBranchAddress("mu_isoLoose", mu_isoLoose, &b_mu_isoLoose);
   fChain->SetBranchAddress("mu_isoTight", mu_isoTight, &b_mu_isoTight);
   fChain->SetBranchAddress("mu_isoGradient", mu_isoGradient, &b_mu_isoGradient);
   fChain->SetBranchAddress("mu_isoGradientLoose", mu_isoGradientLoose, &b_mu_isoGradientLoose);
   fChain->SetBranchAddress("mu_idMedium", mu_idMedium, &b_mu_idMedium);
   fChain->SetBranchAddress("mu_idTight", mu_idTight, &b_mu_idTight);
   fChain->SetBranchAddress("mu_muQuality", mu_muQuality, &b_mu_muQuality);
   fChain->SetBranchAddress("mu_muHighPt", mu_muHighPt, &b_mu_muHighPt);
   fChain->SetBranchAddress("mu_ptcone20", mu_ptcone20, &b_mu_ptcone20);
   fChain->SetBranchAddress("mu_ptcone30", mu_ptcone30, &b_mu_ptcone30);
   fChain->SetBranchAddress("mu_ptcone40", mu_ptcone40, &b_mu_ptcone40);
   fChain->SetBranchAddress("mu_ptvarcone20", mu_ptvarcone20, &b_mu_ptvarcone20);
   fChain->SetBranchAddress("mu_ptvarcone30", mu_ptvarcone30, &b_mu_ptvarcone30);
   fChain->SetBranchAddress("mu_ptvarcone40", mu_ptvarcone40, &b_mu_ptvarcone40);
   fChain->SetBranchAddress("mu_topoetcone20", mu_topoetcone20, &b_mu_topoetcone20);
   fChain->SetBranchAddress("mu_topoetcone30", mu_topoetcone30, &b_mu_topoetcone30);
   fChain->SetBranchAddress("mu_topoetcone40", mu_topoetcone40, &b_mu_topoetcone40);
   fChain->SetBranchAddress("n_ph", &n_ph, &b_n_ph);
   fChain->SetBranchAddress("ph_pt", ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_e", ph_e, &b_ph_e);
   fChain->SetBranchAddress("ph_ptcone20", ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_ptcone30", ph_ptcone30, &b_ph_ptcone30);
   fChain->SetBranchAddress("ph_topoetcone40", ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_isoTight", ph_isoTight, &b_ph_isoTight);
   fChain->SetBranchAddress("ph_particleTruthOrigin", ph_particleTruthOrigin, &b_ph_particleTruthOrigin);
   fChain->SetBranchAddress("ph_particleTruthType", ph_particleTruthType, &b_ph_particleTruthType);
   fChain->SetBranchAddress("n_tau", &n_tau, &b_n_tau);
   fChain->SetBranchAddress("tau_pt", tau_pt, &b_tau_pt);
   fChain->SetBranchAddress("tau_eta", tau_eta, &b_tau_eta);
   fChain->SetBranchAddress("tau_phi", tau_phi, &b_tau_phi);
   fChain->SetBranchAddress("tau_e", tau_e, &b_tau_e);
   fChain->SetBranchAddress("tau_IsLoose", tau_IsLoose, &b_tau_IsLoose);
   fChain->SetBranchAddress("tau_NProng", tau_NProng, &b_tau_NProng);
   fChain->SetBranchAddress("tau_BDTJetScore", tau_BDTJetScore, &b_tau_BDTJetScore);
   fChain->SetBranchAddress("n_fatjet_R10_L0", &n_fatjet_R10_L0, &b_n_fatjet_R10_L0);
   fChain->SetBranchAddress("fatjet_R10_L0_pt", fatjet_R10_L0_pt, &b_fatjet_R10_L0_pt);
   fChain->SetBranchAddress("fatjet_R10_L0_eta", fatjet_R10_L0_eta, &b_fatjet_R10_L0_eta);
   fChain->SetBranchAddress("fatjet_R10_L0_phi", fatjet_R10_L0_phi, &b_fatjet_R10_L0_phi);
   fChain->SetBranchAddress("fatjet_R10_L0_e", fatjet_R10_L0_e, &b_fatjet_R10_L0_e);
   fChain->SetBranchAddress("fatjet_R10_L0_m", fatjet_R10_L0_m, &b_fatjet_R10_L0_m);
   fChain->SetBranchAddress("fatjet_R10_L0_m_calo", fatjet_R10_L0_m_calo, &b_fatjet_R10_L0_m_calo);
   fChain->SetBranchAddress("fatjet_R10_L0_nsmall", fatjet_R10_L0_nsmall, &b_fatjet_R10_L0_nsmall);
   fChain->SetBranchAddress("fatjet_R10_L0_MaxSmallMass", fatjet_R10_L0_MaxSmallMass, &b_fatjet_R10_L0_MaxSmallMass);
   fChain->SetBranchAddress("fatjet_R10_L0_abs_dphi_met", fatjet_R10_L0_abs_dphi_met, &b_fatjet_R10_L0_abs_dphi_met);
   fChain->SetBranchAddress("n_fatjet_mo_R10_L0", &n_fatjet_mo_R10_L0, &b_n_fatjet_mo_R10_L0);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_pt", fatjet_mo_R10_L0_pt, &b_fatjet_mo_R10_L0_pt);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_eta", fatjet_mo_R10_L0_eta, &b_fatjet_mo_R10_L0_eta);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_phi", fatjet_mo_R10_L0_phi, &b_fatjet_mo_R10_L0_phi);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_e", fatjet_mo_R10_L0_e, &b_fatjet_mo_R10_L0_e);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_m", fatjet_mo_R10_L0_m, &b_fatjet_mo_R10_L0_m);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_m_calo", fatjet_mo_R10_L0_m_calo, &b_fatjet_mo_R10_L0_m_calo);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_pt_index", fatjet_mo_R10_L0_pt_index, &b_fatjet_mo_R10_L0_pt_index);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_nsmall", fatjet_mo_R10_L0_nsmall, &b_fatjet_mo_R10_L0_nsmall);
   fChain->SetBranchAddress("fatjet_mo_R10_L0_abs_dphi_met", fatjet_mo_R10_L0_abs_dphi_met, &b_fatjet_mo_R10_L0_abs_dphi_met);
   fChain->SetBranchAddress("n_fatjet_R12_L0", &n_fatjet_R12_L0, &b_n_fatjet_R12_L0);
   fChain->SetBranchAddress("fatjet_R12_L0_pt", fatjet_R12_L0_pt, &b_fatjet_R12_L0_pt);
   fChain->SetBranchAddress("fatjet_R12_L0_eta", fatjet_R12_L0_eta, &b_fatjet_R12_L0_eta);
   fChain->SetBranchAddress("fatjet_R12_L0_phi", fatjet_R12_L0_phi, &b_fatjet_R12_L0_phi);
   fChain->SetBranchAddress("fatjet_R12_L0_e", fatjet_R12_L0_e, &b_fatjet_R12_L0_e);
   fChain->SetBranchAddress("fatjet_R12_L0_m", fatjet_R12_L0_m, &b_fatjet_R12_L0_m);
   fChain->SetBranchAddress("fatjet_R12_L0_m_calo", fatjet_R12_L0_m_calo, &b_fatjet_R12_L0_m_calo);
   fChain->SetBranchAddress("fatjet_R12_L0_nsmall", fatjet_R12_L0_nsmall, &b_fatjet_R12_L0_nsmall);
   fChain->SetBranchAddress("fatjet_R12_L0_MaxSmallMass", fatjet_R12_L0_MaxSmallMass, &b_fatjet_R12_L0_MaxSmallMass);
   fChain->SetBranchAddress("fatjet_R12_L0_abs_dphi_met", fatjet_R12_L0_abs_dphi_met, &b_fatjet_R12_L0_abs_dphi_met);
   fChain->SetBranchAddress("n_fatjet_mo_R12_L0", &n_fatjet_mo_R12_L0, &b_n_fatjet_mo_R12_L0);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_pt", fatjet_mo_R12_L0_pt, &b_fatjet_mo_R12_L0_pt);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_eta", fatjet_mo_R12_L0_eta, &b_fatjet_mo_R12_L0_eta);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_phi", fatjet_mo_R12_L0_phi, &b_fatjet_mo_R12_L0_phi);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_e", fatjet_mo_R12_L0_e, &b_fatjet_mo_R12_L0_e);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_m", fatjet_mo_R12_L0_m, &b_fatjet_mo_R12_L0_m);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_m_calo", fatjet_mo_R12_L0_m_calo, &b_fatjet_mo_R12_L0_m_calo);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_pt_index", fatjet_mo_R12_L0_pt_index, &b_fatjet_mo_R12_L0_pt_index);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_nsmall", fatjet_mo_R12_L0_nsmall, &b_fatjet_mo_R12_L0_nsmall);
   fChain->SetBranchAddress("fatjet_mo_R12_L0_abs_dphi_met", fatjet_mo_R12_L0_abs_dphi_met, &b_fatjet_mo_R12_L0_abs_dphi_met);
   fChain->SetBranchAddress("n_hadtop", &n_hadtop, &b_n_hadtop);
   fChain->SetBranchAddress("hadtop_pt", hadtop_pt, &b_hadtop_pt);
   fChain->SetBranchAddress("hadtop_eta", hadtop_eta, &b_hadtop_eta);
   fChain->SetBranchAddress("hadtop_phi", hadtop_phi, &b_hadtop_phi);
   fChain->SetBranchAddress("hadtop_e", hadtop_e, &b_hadtop_e);
   fChain->SetBranchAddress("hadtop_bjet_index", hadtop_bjet_index, &b_hadtop_bjet_index);
   fChain->SetBranchAddress("hadtop_light_jet1_index", hadtop_light_jet1_index, &b_hadtop_light_jet1_index);
   fChain->SetBranchAddress("hadtop_light_jet2_index", hadtop_light_jet2_index, &b_hadtop_light_jet2_index);
   fChain->SetBranchAddress("hadtop_chi2", hadtop_chi2, &b_hadtop_chi2);
   fChain->SetBranchAddress("hadtop_m", hadtop_m, &b_hadtop_m);
   fChain->SetBranchAddress("n_leptop", &n_leptop, &b_n_leptop);
   fChain->SetBranchAddress("leptop_pt", leptop_pt, &b_leptop_pt);
   fChain->SetBranchAddress("leptop_eta", leptop_eta, &b_leptop_eta);
   fChain->SetBranchAddress("leptop_phi", leptop_phi, &b_leptop_phi);
   fChain->SetBranchAddress("leptop_e", leptop_e, &b_leptop_e);
   fChain->SetBranchAddress("leptop_bjet_index", leptop_bjet_index, &b_leptop_bjet_index);
   fChain->SetBranchAddress("n_truth_matched_jet", &n_truth_matched_jet, &b_n_truth_matched_jet);
   fChain->SetBranchAddress("truth_matched_jet_pt", &truth_matched_jet_pt, &b_truth_matched_jet_pt);
   fChain->SetBranchAddress("truth_matched_jet_eta", &truth_matched_jet_eta, &b_truth_matched_jet_eta);
   fChain->SetBranchAddress("truth_matched_jet_phi", &truth_matched_jet_phi, &b_truth_matched_jet_phi);
   fChain->SetBranchAddress("truth_matched_jet_e", &truth_matched_jet_e, &b_truth_matched_jet_e);
   fChain->SetBranchAddress("truth_matched_jet_n_matches", &truth_matched_jet_n_matches, &b_truth_matched_jet_n_matches);
   fChain->SetBranchAddress("n_rrtop", &n_rrtop, &b_n_rrtop);
   fChain->SetBranchAddress("rrtop_pt", rrtop_pt, &b_rrtop_pt);
   fChain->SetBranchAddress("rrtop_eta", rrtop_eta, &b_rrtop_eta);
   fChain->SetBranchAddress("rrtop_phi", rrtop_phi, &b_rrtop_phi);
   fChain->SetBranchAddress("rrtop_e", rrtop_e, &b_rrtop_e);
   fChain->SetBranchAddress("rrtop_m", rrtop_m, &b_rrtop_m);
   fChain->SetBranchAddress("rrtop_m_calo", rrtop_m_calo, &b_rrtop_m_calo);
   fChain->SetBranchAddress("rrtop_r", rrtop_r, &b_rrtop_r);
   fChain->SetBranchAddress("rrtop_n_constit", rrtop_n_constit, &b_rrtop_n_constit);
   fChain->SetBranchAddress("rrtop_n_btag", rrtop_n_btag, &b_rrtop_n_btag);
   fChain->SetBranchAddress("rrtop_top_match", rrtop_top_match, &b_rrtop_top_match);
   fChain->SetBranchAddress("rrtop_cand_match", rrtop_cand_match, &b_rrtop_cand_match);
   fChain->SetBranchAddress("rrtop_jets_match", rrtop_jets_match, &b_rrtop_jets_match);
   fChain->SetBranchAddress("rrtop_bjet_match", rrtop_bjet_match, &b_rrtop_bjet_match);
   fChain->SetBranchAddress("rrtop_ljet1_match", rrtop_ljet1_match, &b_rrtop_ljet1_match);
   fChain->SetBranchAddress("rrtop_ljet2_match", rrtop_ljet2_match, &b_rrtop_ljet2_match);
   fChain->SetBranchAddress("rrtop_h0_pt", rrtop_h0_pt, &b_rrtop_h0_pt);
   fChain->SetBranchAddress("rrtop_h0_r", rrtop_h0_r, &b_rrtop_h0_r);
   fChain->SetBranchAddress("rrtop_h0_n", rrtop_h0_n, &b_rrtop_h0_n);
   fChain->SetBranchAddress("rrtop_h0_match", rrtop_h0_match, &b_rrtop_h0_match);
   fChain->SetBranchAddress("rrtop_h0_status", rrtop_h0_status, &b_rrtop_h0_status);
   fChain->SetBranchAddress("rrtop_h1_pt", rrtop_h1_pt, &b_rrtop_h1_pt);
   fChain->SetBranchAddress("rrtop_h1_r", rrtop_h1_r, &b_rrtop_h1_r);
   fChain->SetBranchAddress("rrtop_h1_n", rrtop_h1_n, &b_rrtop_h1_n);
   fChain->SetBranchAddress("rrtop_h1_match", rrtop_h1_match, &b_rrtop_h1_match);
   fChain->SetBranchAddress("rrtop_h1_status", rrtop_h1_status, &b_rrtop_h1_status);
   fChain->SetBranchAddress("rrtop_h2_pt", rrtop_h2_pt, &b_rrtop_h2_pt);
   fChain->SetBranchAddress("rrtop_h2_r", rrtop_h2_r, &b_rrtop_h2_r);
   fChain->SetBranchAddress("rrtop_h2_n", rrtop_h2_n, &b_rrtop_h2_n);
   fChain->SetBranchAddress("rrtop_h2_match", rrtop_h2_match, &b_rrtop_h2_match);
   fChain->SetBranchAddress("rrtop_h2_status", rrtop_h2_status, &b_rrtop_h2_status);
   fChain->SetBranchAddress("n_bad_rrtop", &n_bad_rrtop, &b_n_bad_rrtop);
   fChain->SetBranchAddress("bad_rrtop_pt", bad_rrtop_pt, &b_bad_rrtop_pt);
   fChain->SetBranchAddress("bad_rrtop_eta", bad_rrtop_eta, &b_bad_rrtop_eta);
   fChain->SetBranchAddress("bad_rrtop_phi", bad_rrtop_phi, &b_bad_rrtop_phi);
   fChain->SetBranchAddress("bad_rrtop_e", bad_rrtop_e, &b_bad_rrtop_e);
   fChain->SetBranchAddress("bad_rrtop_m", bad_rrtop_m, &b_bad_rrtop_m);
   fChain->SetBranchAddress("bad_rrtop_r", bad_rrtop_r, &b_bad_rrtop_r);
   fChain->SetBranchAddress("bad_rrtop_n_constit", bad_rrtop_n_constit, &b_bad_rrtop_n_constit);
   fChain->SetBranchAddress("bad_rrtop_n_btag", bad_rrtop_n_btag, &b_bad_rrtop_n_btag);
   fChain->SetBranchAddress("bad_rrtop_top_match", bad_rrtop_top_match, &b_bad_rrtop_top_match);
   fChain->SetBranchAddress("bad_rrtop_cand_match", bad_rrtop_cand_match, &b_bad_rrtop_cand_match);
   fChain->SetBranchAddress("bad_rrtop_jets_match", bad_rrtop_jets_match, &b_bad_rrtop_jets_match);
   fChain->SetBranchAddress("bad_rrtop_bjet_match", bad_rrtop_bjet_match, &b_bad_rrtop_bjet_match);
   fChain->SetBranchAddress("bad_rrtop_ljet1_match", bad_rrtop_ljet1_match, &b_bad_rrtop_ljet1_match);
   fChain->SetBranchAddress("bad_rrtop_ljet2_match", bad_rrtop_ljet2_match, &b_bad_rrtop_ljet2_match);
   fChain->SetBranchAddress("bad_rrtop_h0_pt", bad_rrtop_h0_pt, &b_bad_rrtop_h0_pt);
   fChain->SetBranchAddress("bad_rrtop_h0_r", bad_rrtop_h0_r, &b_bad_rrtop_h0_r);
   fChain->SetBranchAddress("bad_rrtop_h0_n", bad_rrtop_h0_n, &b_bad_rrtop_h0_n);
   fChain->SetBranchAddress("bad_rrtop_h0_match", bad_rrtop_h0_match, &b_bad_rrtop_h0_match);
   fChain->SetBranchAddress("bad_rrtop_h0_status", bad_rrtop_h0_status, &b_bad_rrtop_h0_status);
   fChain->SetBranchAddress("bad_rrtop_h1_pt", bad_rrtop_h1_pt, &b_bad_rrtop_h1_pt);
   fChain->SetBranchAddress("bad_rrtop_h1_r", bad_rrtop_h1_r, &b_bad_rrtop_h1_r);
   fChain->SetBranchAddress("bad_rrtop_h1_n", bad_rrtop_h1_n, &b_bad_rrtop_h1_n);
   fChain->SetBranchAddress("bad_rrtop_h1_match", bad_rrtop_h1_match, &b_bad_rrtop_h1_match);
   fChain->SetBranchAddress("bad_rrtop_h1_status", bad_rrtop_h1_status, &b_bad_rrtop_h1_status);
   fChain->SetBranchAddress("bad_rrtop_h2_pt", bad_rrtop_h2_pt, &b_bad_rrtop_h2_pt);
   fChain->SetBranchAddress("bad_rrtop_h2_r", bad_rrtop_h2_r, &b_bad_rrtop_h2_r);
   fChain->SetBranchAddress("bad_rrtop_h2_n", bad_rrtop_h2_n, &b_bad_rrtop_h2_n);
   fChain->SetBranchAddress("bad_rrtop_h2_match", bad_rrtop_h2_match, &b_bad_rrtop_h2_match);
   fChain->SetBranchAddress("bad_rrtop_h2_status", bad_rrtop_h2_status, &b_bad_rrtop_h2_status);
   fChain->SetBranchAddress("n_hadtop_cand", &n_hadtop_cand, &b_n_hadtop_cand);
   fChain->SetBranchAddress("hadtop_cand_pt", hadtop_cand_pt, &b_hadtop_cand_pt);
   fChain->SetBranchAddress("hadtop_cand_eta", hadtop_cand_eta, &b_hadtop_cand_eta);
   fChain->SetBranchAddress("hadtop_cand_phi", hadtop_cand_phi, &b_hadtop_cand_phi);
   fChain->SetBranchAddress("hadtop_cand_e", hadtop_cand_e, &b_hadtop_cand_e);
   fChain->SetBranchAddress("hadtop_cand_m", hadtop_cand_m, &b_hadtop_cand_m);
   fChain->SetBranchAddress("hadtop_cand_m_calo", hadtop_cand_m_calo, &b_hadtop_cand_m_calo);
   fChain->SetBranchAddress("hadtop_cand_r", hadtop_cand_r, &b_hadtop_cand_r);
   fChain->SetBranchAddress("hadtop_cand_n_constit", hadtop_cand_n_constit, &b_hadtop_cand_n_constit);
   fChain->SetBranchAddress("hadtop_cand_n_btag", hadtop_cand_n_btag, &b_hadtop_cand_n_btag);
   fChain->SetBranchAddress("hadtop_cand_top_match", hadtop_cand_top_match, &b_hadtop_cand_top_match);
   fChain->SetBranchAddress("hadtop_cand_cand_match", hadtop_cand_cand_match, &b_hadtop_cand_cand_match);
   fChain->SetBranchAddress("hadtop_cand_jets_match", hadtop_cand_jets_match, &b_hadtop_cand_jets_match);
   fChain->SetBranchAddress("hadtop_cand_bjet_match", hadtop_cand_bjet_match, &b_hadtop_cand_bjet_match);
   fChain->SetBranchAddress("hadtop_cand_ljet1_match", hadtop_cand_ljet1_match, &b_hadtop_cand_ljet1_match);
   fChain->SetBranchAddress("hadtop_cand_ljet2_match", hadtop_cand_ljet2_match, &b_hadtop_cand_ljet2_match);
   fChain->SetBranchAddress("n_truth_photon", &n_truth_photon, &b_n_truth_photon);
   fChain->SetBranchAddress("truth_photon_pt", &truth_photon_pt, &b_truth_photon_pt);
   fChain->SetBranchAddress("truth_photon_eta", &truth_photon_eta, &b_truth_photon_eta);
   fChain->SetBranchAddress("truth_photon_phi", &truth_photon_phi, &b_truth_photon_phi);
   fChain->SetBranchAddress("truth_photon_e", &truth_photon_e, &b_truth_photon_e);
   fChain->SetBranchAddress("truth_photon_motherPdgId", &truth_photon_motherPdgId, &b_truth_photon_motherPdgId);
   fChain->SetBranchAddress("truth_photon_statusCode", &truth_photon_statusCode, &b_truth_photon_statusCode);
   fChain->SetBranchAddress("truth_photon_barCode", &truth_photon_barCode, &b_truth_photon_barCode);
   fChain->SetBranchAddress("n_truth_top", &n_truth_top, &b_n_truth_top);
   fChain->SetBranchAddress("truth_top_pt", &truth_top_pt, &b_truth_top_pt);
   fChain->SetBranchAddress("truth_top_eta", &truth_top_eta, &b_truth_top_eta);
   fChain->SetBranchAddress("truth_top_phi", &truth_top_phi, &b_truth_top_phi);
   fChain->SetBranchAddress("truth_top_e", &truth_top_e, &b_truth_top_e);
   fChain->SetBranchAddress("met", &met, &b_met);
   fChain->SetBranchAddress("met_x", &met_x, &b_met_x);
   fChain->SetBranchAddress("met_y", &met_y, &b_met_y);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("met_sumet", &met_sumet, &b_met_sumet);
   fChain->SetBranchAddress("met_el", &met_el, &b_met_el);
   fChain->SetBranchAddress("met_el_x", &met_el_x, &b_met_el_x);
   fChain->SetBranchAddress("met_el_y", &met_el_y, &b_met_el_y);
   fChain->SetBranchAddress("met_el_phi", &met_el_phi, &b_met_el_phi);
   fChain->SetBranchAddress("met_el_sumet", &met_el_sumet, &b_met_el_sumet);
   fChain->SetBranchAddress("met_mu", &met_mu, &b_met_mu);
   fChain->SetBranchAddress("met_mu_x", &met_mu_x, &b_met_mu_x);
   fChain->SetBranchAddress("met_mu_y", &met_mu_y, &b_met_mu_y);
   fChain->SetBranchAddress("met_mu_phi", &met_mu_phi, &b_met_mu_phi);
   fChain->SetBranchAddress("met_mu_sumet", &met_mu_sumet, &b_met_mu_sumet);
   fChain->SetBranchAddress("met_jet", &met_jet, &b_met_jet);
   fChain->SetBranchAddress("met_jet_x", &met_jet_x, &b_met_jet_x);
   fChain->SetBranchAddress("met_jet_y", &met_jet_y, &b_met_jet_y);
   fChain->SetBranchAddress("met_jet_phi", &met_jet_phi, &b_met_jet_phi);
   fChain->SetBranchAddress("met_jet_sumet", &met_jet_sumet, &b_met_jet_sumet);
   fChain->SetBranchAddress("met_soft", &met_soft, &b_met_soft);
   fChain->SetBranchAddress("met_soft_x", &met_soft_x, &b_met_soft_x);
   fChain->SetBranchAddress("met_soft_y", &met_soft_y, &b_met_soft_y);
   fChain->SetBranchAddress("met_soft_phi", &met_soft_phi, &b_met_soft_phi);
   fChain->SetBranchAddress("met_soft_sumet", &met_soft_sumet, &b_met_soft_sumet);
   fChain->SetBranchAddress("met_cst", &met_cst, &b_met_cst);
   fChain->SetBranchAddress("met_cst_x", &met_cst_x, &b_met_cst_x);
   fChain->SetBranchAddress("met_cst_y", &met_cst_y, &b_met_cst_y);
   fChain->SetBranchAddress("met_cst_phi", &met_cst_phi, &b_met_cst_phi);
   fChain->SetBranchAddress("met_cst_sumet", &met_cst_sumet, &b_met_cst_sumet);
   fChain->SetBranchAddress("met_cst_soft", &met_cst_soft, &b_met_cst_soft);
   fChain->SetBranchAddress("met_photons", &met_photons, &b_met_photons);
   fChain->SetBranchAddress("met_photons_x", &met_photons_x, &b_met_photons_x);
   fChain->SetBranchAddress("met_photons_y", &met_photons_y, &b_met_photons_y);
   fChain->SetBranchAddress("met_photons_phi", &met_photons_phi, &b_met_photons_phi);
   fChain->SetBranchAddress("met_photons_sumet", &met_photons_sumet, &b_met_photons_sumet);
   fChain->SetBranchAddress("mc_event_weight", &mc_event_weight, &b_mc_event_weight);
   fChain->SetBranchAddress("weight", &weight, &b_weight);
   fChain->SetBranchAddress("sf_btag", &sf_btag, &b_sf_btag);
   fChain->SetBranchAddress("sf_jvt", &sf_jvt, &b_sf_jvt);
   fChain->SetBranchAddress("sf_el", &sf_el, &b_sf_el);
   fChain->SetBranchAddress("sf_mu", &sf_mu, &b_sf_mu);
   fChain->SetBranchAddress("sf_ph", &sf_ph, &b_sf_ph);
   fChain->SetBranchAddress("sf_el_trigger", &sf_el_trigger, &b_sf_el_trigger);
   fChain->SetBranchAddress("sf_mu_trigger", &sf_mu_trigger, &b_sf_mu_trigger);
   fChain->SetBranchAddress("sf_total", &sf_total, &b_sf_total);
   fChain->SetBranchAddress("sf_no_pu", &sf_no_pu, &b_sf_no_pu);
   fChain->SetBranchAddress("pileup_weight", &pileup_weight, &b_pileup_weight);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_total__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_no_pu__SYST_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_total__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_no_pu__SYST_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_total__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_no_pu__SYST_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_total__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_no_pu__SYST_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_total__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_sf_no_pu__SYST_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_B_systematics__1down", &sf_total__SYST_FT_EFF_B_systematics__1down, &b_sf_total__SYST_FT_EFF_B_systematics__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_B_systematics__1down", &sf_no_pu__SYST_FT_EFF_B_systematics__1down, &b_sf_no_pu__SYST_FT_EFF_B_systematics__1down);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_B_systematics__1up", &sf_total__SYST_FT_EFF_B_systematics__1up, &b_sf_total__SYST_FT_EFF_B_systematics__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_B_systematics__1up", &sf_no_pu__SYST_FT_EFF_B_systematics__1up, &b_sf_no_pu__SYST_FT_EFF_B_systematics__1up);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_C_systematics__1down", &sf_total__SYST_FT_EFF_C_systematics__1down, &b_sf_total__SYST_FT_EFF_C_systematics__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_C_systematics__1down", &sf_no_pu__SYST_FT_EFF_C_systematics__1down, &b_sf_no_pu__SYST_FT_EFF_C_systematics__1down);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_C_systematics__1up", &sf_total__SYST_FT_EFF_C_systematics__1up, &b_sf_total__SYST_FT_EFF_C_systematics__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_C_systematics__1up", &sf_no_pu__SYST_FT_EFF_C_systematics__1up, &b_sf_no_pu__SYST_FT_EFF_C_systematics__1up);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_Light_systematics__1down", &sf_total__SYST_FT_EFF_Light_systematics__1down, &b_sf_total__SYST_FT_EFF_Light_systematics__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_Light_systematics__1down", &sf_no_pu__SYST_FT_EFF_Light_systematics__1down, &b_sf_no_pu__SYST_FT_EFF_Light_systematics__1down);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_Light_systematics__1up", &sf_total__SYST_FT_EFF_Light_systematics__1up, &b_sf_total__SYST_FT_EFF_Light_systematics__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_Light_systematics__1up", &sf_no_pu__SYST_FT_EFF_Light_systematics__1up, &b_sf_no_pu__SYST_FT_EFF_Light_systematics__1up);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_extrapolation__1down", &sf_total__SYST_FT_EFF_extrapolation__1down, &b_sf_total__SYST_FT_EFF_extrapolation__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_extrapolation__1down", &sf_no_pu__SYST_FT_EFF_extrapolation__1down, &b_sf_no_pu__SYST_FT_EFF_extrapolation__1down);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_extrapolation__1up", &sf_total__SYST_FT_EFF_extrapolation__1up, &b_sf_total__SYST_FT_EFF_extrapolation__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_extrapolation__1up", &sf_no_pu__SYST_FT_EFF_extrapolation__1up, &b_sf_no_pu__SYST_FT_EFF_extrapolation__1up);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_extrapolation_from_charm__1down", &sf_total__SYST_FT_EFF_extrapolation_from_charm__1down, &b_sf_total__SYST_FT_EFF_extrapolation_from_charm__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1down", &sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1down, &b_sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1down);
   fChain->SetBranchAddress("sf_total__SYST_FT_EFF_extrapolation_from_charm__1up", &sf_total__SYST_FT_EFF_extrapolation_from_charm__1up, &b_sf_total__SYST_FT_EFF_extrapolation_from_charm__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1up", &sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1up, &b_sf_no_pu__SYST_FT_EFF_extrapolation_from_charm__1up);
   fChain->SetBranchAddress("sf_total__SYST_JET_JvtEfficiency__1down", &sf_total__SYST_JET_JvtEfficiency__1down, &b_sf_total__SYST_JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_JET_JvtEfficiency__1down", &sf_no_pu__SYST_JET_JvtEfficiency__1down, &b_sf_no_pu__SYST_JET_JvtEfficiency__1down);
   fChain->SetBranchAddress("sf_total__SYST_JET_JvtEfficiency__1up", &sf_total__SYST_JET_JvtEfficiency__1up, &b_sf_total__SYST_JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_JET_JvtEfficiency__1up", &sf_no_pu__SYST_JET_JvtEfficiency__1up, &b_sf_no_pu__SYST_JET_JvtEfficiency__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_STAT__1down", &sf_total__SYST_MUON_EFF_STAT__1down, &b_sf_total__SYST_MUON_EFF_STAT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_STAT__1down", &sf_no_pu__SYST_MUON_EFF_STAT__1down, &b_sf_no_pu__SYST_MUON_EFF_STAT__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_STAT__1up", &sf_total__SYST_MUON_EFF_STAT__1up, &b_sf_total__SYST_MUON_EFF_STAT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_STAT__1up", &sf_no_pu__SYST_MUON_EFF_STAT__1up, &b_sf_no_pu__SYST_MUON_EFF_STAT__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_STAT_LOWPT__1down", &sf_total__SYST_MUON_EFF_STAT_LOWPT__1down, &b_sf_total__SYST_MUON_EFF_STAT_LOWPT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1down", &sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1down, &b_sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_STAT_LOWPT__1up", &sf_total__SYST_MUON_EFF_STAT_LOWPT__1up, &b_sf_total__SYST_MUON_EFF_STAT_LOWPT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1up", &sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1up, &b_sf_no_pu__SYST_MUON_EFF_STAT_LOWPT__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_SYS__1down", &sf_total__SYST_MUON_EFF_SYS__1down, &b_sf_total__SYST_MUON_EFF_SYS__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_SYS__1down", &sf_no_pu__SYST_MUON_EFF_SYS__1down, &b_sf_no_pu__SYST_MUON_EFF_SYS__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_SYS__1up", &sf_total__SYST_MUON_EFF_SYS__1up, &b_sf_total__SYST_MUON_EFF_SYS__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_SYS__1up", &sf_no_pu__SYST_MUON_EFF_SYS__1up, &b_sf_no_pu__SYST_MUON_EFF_SYS__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_SYS_LOWPT__1down", &sf_total__SYST_MUON_EFF_SYS_LOWPT__1down, &b_sf_total__SYST_MUON_EFF_SYS_LOWPT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1down", &sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1down, &b_sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_SYS_LOWPT__1up", &sf_total__SYST_MUON_EFF_SYS_LOWPT__1up, &b_sf_total__SYST_MUON_EFF_SYS_LOWPT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1up", &sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1up, &b_sf_no_pu__SYST_MUON_EFF_SYS_LOWPT__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_TrigStatUncertainty__1down", &sf_total__SYST_MUON_EFF_TrigStatUncertainty__1down, &b_sf_total__SYST_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1down", &sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1down, &b_sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_TrigStatUncertainty__1up", &sf_total__SYST_MUON_EFF_TrigStatUncertainty__1up, &b_sf_total__SYST_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1up", &sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1up, &b_sf_no_pu__SYST_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_TrigSystUncertainty__1down", &sf_total__SYST_MUON_EFF_TrigSystUncertainty__1down, &b_sf_total__SYST_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1down", &sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1down, &b_sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_EFF_TrigSystUncertainty__1up", &sf_total__SYST_MUON_EFF_TrigSystUncertainty__1up, &b_sf_total__SYST_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1up", &sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1up, &b_sf_no_pu__SYST_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_ISO_STAT__1down", &sf_total__SYST_MUON_ISO_STAT__1down, &b_sf_total__SYST_MUON_ISO_STAT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_ISO_STAT__1down", &sf_no_pu__SYST_MUON_ISO_STAT__1down, &b_sf_no_pu__SYST_MUON_ISO_STAT__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_ISO_STAT__1up", &sf_total__SYST_MUON_ISO_STAT__1up, &b_sf_total__SYST_MUON_ISO_STAT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_ISO_STAT__1up", &sf_no_pu__SYST_MUON_ISO_STAT__1up, &b_sf_no_pu__SYST_MUON_ISO_STAT__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_ISO_SYS__1down", &sf_total__SYST_MUON_ISO_SYS__1down, &b_sf_total__SYST_MUON_ISO_SYS__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_ISO_SYS__1down", &sf_no_pu__SYST_MUON_ISO_SYS__1down, &b_sf_no_pu__SYST_MUON_ISO_SYS__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_ISO_SYS__1up", &sf_total__SYST_MUON_ISO_SYS__1up, &b_sf_total__SYST_MUON_ISO_SYS__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_ISO_SYS__1up", &sf_no_pu__SYST_MUON_ISO_SYS__1up, &b_sf_no_pu__SYST_MUON_ISO_SYS__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_TTVA_STAT__1down", &sf_total__SYST_MUON_TTVA_STAT__1down, &b_sf_total__SYST_MUON_TTVA_STAT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_TTVA_STAT__1down", &sf_no_pu__SYST_MUON_TTVA_STAT__1down, &b_sf_no_pu__SYST_MUON_TTVA_STAT__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_TTVA_STAT__1up", &sf_total__SYST_MUON_TTVA_STAT__1up, &b_sf_total__SYST_MUON_TTVA_STAT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_TTVA_STAT__1up", &sf_no_pu__SYST_MUON_TTVA_STAT__1up, &b_sf_no_pu__SYST_MUON_TTVA_STAT__1up);
   fChain->SetBranchAddress("sf_total__SYST_MUON_TTVA_SYS__1down", &sf_total__SYST_MUON_TTVA_SYS__1down, &b_sf_total__SYST_MUON_TTVA_SYS__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_TTVA_SYS__1down", &sf_no_pu__SYST_MUON_TTVA_SYS__1down, &b_sf_no_pu__SYST_MUON_TTVA_SYS__1down);
   fChain->SetBranchAddress("sf_total__SYST_MUON_TTVA_SYS__1up", &sf_total__SYST_MUON_TTVA_SYS__1up, &b_sf_total__SYST_MUON_TTVA_SYS__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_MUON_TTVA_SYS__1up", &sf_no_pu__SYST_MUON_TTVA_SYS__1up, &b_sf_no_pu__SYST_MUON_TTVA_SYS__1up);
   fChain->SetBranchAddress("sf_total__SYST_PRW_DATASF__1down", &sf_total__SYST_PRW_DATASF__1down, &b_sf_total__SYST_PRW_DATASF__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_PRW_DATASF__1down", &sf_no_pu__SYST_PRW_DATASF__1down, &b_sf_no_pu__SYST_PRW_DATASF__1down);
   fChain->SetBranchAddress("sf_total__SYST_PRW_DATASF__1up", &sf_total__SYST_PRW_DATASF__1up, &b_sf_total__SYST_PRW_DATASF__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_PRW_DATASF__1up", &sf_no_pu__SYST_PRW_DATASF__1up, &b_sf_no_pu__SYST_PRW_DATASF__1up);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1down);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_JETID_TOTAL__1up);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
   fChain->SetBranchAddress("sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_sf_total__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
   fChain->SetBranchAddress("sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_sf_no_pu__SYST_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
   fChain->SetBranchAddress("av_int_per_xing", &av_int_per_xing, &b_av_int_per_xing);
   fChain->SetBranchAddress("num_pv", &num_pv, &b_num_pv);
   fChain->SetBranchAddress("neutrino_met", &neutrino_met, &b_neutrino_met);
   fChain->SetBranchAddress("gen_filter_met", &gen_filter_met, &b_gen_filter_met);
   fChain->SetBranchAddress("weight_stop_reweighting_right", &weight_stop_reweighting_right, &b_weight_stop_reweighting_right);
   fChain->SetBranchAddress("weight_stop_reweighting_left", &weight_stop_reweighting_left, &b_weight_stop_reweighting_left);
   fChain->SetBranchAddress("weight_bC_reweighting_rightright", &weight_bC_reweighting_rightright, &b_weight_bC_reweighting_rightright);
   fChain->SetBranchAddress("weight_bC_reweighting_leftleft", &weight_bC_reweighting_leftleft, &b_weight_bC_reweighting_leftleft);
   fChain->SetBranchAddress("weight_bC_reweighting_rightleft", &weight_bC_reweighting_rightleft, &b_weight_bC_reweighting_rightleft);
   fChain->SetBranchAddress("weight_bC_reweighting_leftright", &weight_bC_reweighting_leftright, &b_weight_bC_reweighting_leftright);
   fChain->SetBranchAddress("weight_sherpa22_njets", &weight_sherpa22_njets, &b_weight_sherpa22_njets);
   fChain->SetBranchAddress("met_truth", &met_truth, &b_met_truth);
   fChain->SetBranchAddress("met_dphi", &met_dphi, &b_met_dphi);
   fChain->SetBranchAddress("dphi_met_fatjet0_R10_L0", &dphi_met_fatjet0_R10_L0, &b_dphi_met_fatjet0_R10_L0);
   fChain->SetBranchAddress("dphi_met_fatjet1_R10_L0", &dphi_met_fatjet1_R10_L0, &b_dphi_met_fatjet1_R10_L0);
   fChain->SetBranchAddress("dphi_met_fatjet0_R12_L0", &dphi_met_fatjet0_R12_L0, &b_dphi_met_fatjet0_R12_L0);
   fChain->SetBranchAddress("dphi_met_fatjet1_R12_L0", &dphi_met_fatjet1_R12_L0, &b_dphi_met_fatjet1_R12_L0);
   fChain->SetBranchAddress("mt", &mt, &b_mt);
   fChain->SetBranchAddress("blinding", &blinding, &b_blinding);
   fChain->SetBranchAddress("dr_qq", &dr_qq, &b_dr_qq);
   fChain->SetBranchAddress("min_dr_bq", &min_dr_bq, &b_min_dr_bq);
   fChain->SetBranchAddress("max_dr_bq", &max_dr_bq, &b_max_dr_bq);
   fChain->SetBranchAddress("truth_hadtop_pt", &truth_hadtop_pt, &b_truth_hadtop_pt);
   fChain->SetBranchAddress("truth_hadtop_eta", &truth_hadtop_eta, &b_truth_hadtop_eta);
   fChain->SetBranchAddress("truth_hadtop_phi", &truth_hadtop_phi, &b_truth_hadtop_phi);
   fChain->SetBranchAddress("truth_hadtop_e", &truth_hadtop_e, &b_truth_hadtop_e);
   fChain->SetBranchAddress("truth_hadtop_b_pt", &truth_hadtop_b_pt, &b_truth_hadtop_b_pt);
   fChain->SetBranchAddress("truth_hadtop_b_eta", &truth_hadtop_b_eta, &b_truth_hadtop_b_eta);
   fChain->SetBranchAddress("truth_hadtop_b_phi", &truth_hadtop_b_phi, &b_truth_hadtop_b_phi);
   fChain->SetBranchAddress("truth_hadtop_b_e", &truth_hadtop_b_e, &b_truth_hadtop_b_e);
   fChain->SetBranchAddress("truth_hadtop_q1_pt", &truth_hadtop_q1_pt, &b_truth_hadtop_q1_pt);
   fChain->SetBranchAddress("truth_hadtop_q1_eta", &truth_hadtop_q1_eta, &b_truth_hadtop_q1_eta);
   fChain->SetBranchAddress("truth_hadtop_q1_phi", &truth_hadtop_q1_phi, &b_truth_hadtop_q1_phi);
   fChain->SetBranchAddress("truth_hadtop_q1_e", &truth_hadtop_q1_e, &b_truth_hadtop_q1_e);
   fChain->SetBranchAddress("truth_hadtop_q2_pt", &truth_hadtop_q2_pt, &b_truth_hadtop_q2_pt);
   fChain->SetBranchAddress("truth_hadtop_q2_eta", &truth_hadtop_q2_eta, &b_truth_hadtop_q2_eta);
   fChain->SetBranchAddress("truth_hadtop_q2_phi", &truth_hadtop_q2_phi, &b_truth_hadtop_q2_phi);
   fChain->SetBranchAddress("truth_hadtop_q2_e", &truth_hadtop_q2_e, &b_truth_hadtop_q2_e);
   fChain->SetBranchAddress("dphi_jet0_ptmiss", &dphi_jet0_ptmiss, &b_dphi_jet0_ptmiss);
   fChain->SetBranchAddress("dphi_jet1_ptmiss", &dphi_jet1_ptmiss, &b_dphi_jet1_ptmiss);
   fChain->SetBranchAddress("dphi_jet2_ptmiss", &dphi_jet2_ptmiss, &b_dphi_jet2_ptmiss);
   fChain->SetBranchAddress("dphi_jet3_ptmiss", &dphi_jet3_ptmiss, &b_dphi_jet3_ptmiss);
   fChain->SetBranchAddress("dphi_min_ptmiss", &dphi_min_ptmiss, &b_dphi_min_ptmiss);
   fChain->SetBranchAddress("dphi_met_lep", &dphi_met_lep, &b_dphi_met_lep);
   fChain->SetBranchAddress("dphi_b_lep_min", &dphi_b_lep_min, &b_dphi_b_lep_min);
   fChain->SetBranchAddress("dphi_b_lep_max", &dphi_b_lep_max, &b_dphi_b_lep_max);
   fChain->SetBranchAddress("dphi_b_ptmiss_min", &dphi_b_ptmiss_min, &b_dphi_b_ptmiss_min);
   fChain->SetBranchAddress("dphi_b_ptmiss_max", &dphi_b_ptmiss_max, &b_dphi_b_ptmiss_max);
   fChain->SetBranchAddress("met_proj_lep", &met_proj_lep, &b_met_proj_lep);
   fChain->SetBranchAddress("lepPt_over_met", &lepPt_over_met, &b_lepPt_over_met);
   fChain->SetBranchAddress("lepPt_over_met_lep", &lepPt_over_met_lep, &b_lepPt_over_met_lep);
   fChain->SetBranchAddress("ht", &ht, &b_ht);
   fChain->SetBranchAddress("met_sig", &met_sig, &b_met_sig);
   fChain->SetBranchAddress("m_top", &m_top, &b_m_top);
   fChain->SetBranchAddress("m_top_chi2", &m_top_chi2, &b_m_top_chi2);
   fChain->SetBranchAddress("m_top_chi2_chi2", &m_top_chi2_chi2, &b_m_top_chi2_chi2);
   fChain->SetBranchAddress("dr_bjet_lep", &dr_bjet_lep, &b_dr_bjet_lep);
   fChain->SetBranchAddress("mindr_bjet_lep", &mindr_bjet_lep, &b_mindr_bjet_lep);
   fChain->SetBranchAddress("top_dr_bjet_lep", &top_dr_bjet_lep, &b_top_dr_bjet_lep);
   fChain->SetBranchAddress("amt2", &amt2, &b_amt2);
   fChain->SetBranchAddress("mt2_tau", &mt2_tau, &b_mt2_tau);
   fChain->SetBranchAddress("mt2stop", &mt2stop, &b_mt2stop);
   fChain->SetBranchAddress("mT2tauLooseTau_GeV", &mT2tauLooseTau_GeV, &b_mT2tauLooseTau_GeV);
   fChain->SetBranchAddress("topness", &topness, &b_topness);
   fChain->SetBranchAddress("photon_mt", &photon_mt, &b_photon_mt);
   fChain->SetBranchAddress("photon_met", &photon_met, &b_photon_met);
   fChain->SetBranchAddress("ht_sig", &ht_sig, &b_ht_sig);
   fChain->SetBranchAddress("ht_sig_num", &ht_sig_num, &b_ht_sig_num);
   fChain->SetBranchAddress("ht_sig_denom", &ht_sig_denom, &b_ht_sig_denom);
   fChain->SetBranchAddress("ht_sig_sumet", &ht_sig_sumet, &b_ht_sig_sumet);
   fChain->SetBranchAddress("ht_sig_dphi", &ht_sig_dphi, &b_ht_sig_dphi);
   fChain->SetBranchAddress("ht_sig_dphi_reco", &ht_sig_dphi_reco, &b_ht_sig_dphi_reco);
   fChain->SetBranchAddress("ht_sig_photon", &ht_sig_photon, &b_ht_sig_photon);
   fChain->SetBranchAddress("deltaRbb_ordermv2", &deltaRbb_ordermv2, &b_deltaRbb_ordermv2);
   fChain->SetBranchAddress("deltaRbb_orderpt", &deltaRbb_orderpt, &b_deltaRbb_orderpt);
   fChain->SetBranchAddress("m_bl", &m_bl, &b_m_bl);
   fChain->SetBranchAddress("m_b1l", &m_b1l, &b_m_b1l);
   fChain->SetBranchAddress("m_b2l", &m_b2l, &b_m_b2l);
   fChain->SetBranchAddress("m_bj", &m_bj, &b_m_bj);
   fChain->SetBranchAddress("m_b1j", &m_b1j, &b_m_b1j);
   fChain->SetBranchAddress("m_b2j", &m_b2j, &b_m_b2j);
   fChain->SetBranchAddress("mT_blMET", &mT_blMET, &b_mT_blMET);
   fChain->SetBranchAddress("mT_b1lMET", &mT_b1lMET, &b_mT_b1lMET);
   fChain->SetBranchAddress("mT_b2lMET", &mT_b2lMET, &b_mT_b2lMET);
   fChain->SetBranchAddress("sumPt_b1b2", &sumPt_b1b2, &b_sumPt_b1b2);
   fChain->SetBranchAddress("m_bb", &m_bb, &b_m_bb);
   fChain->SetBranchAddress("m_bl_max", &m_bl_max, &b_m_bl_max);
   fChain->SetBranchAddress("m_bl_min", &m_bl_min, &b_m_bl_min);
   fChain->SetBranchAddress("mT_blMET_max", &mT_blMET_max, &b_mT_blMET_max);
   fChain->SetBranchAddress("mT_blMET_min", &mT_blMET_min, &b_mT_blMET_min);
   fChain->SetBranchAddress("nnlo_weight", &nnlo_weight, &b_nnlo_weight);
   fChain->SetBranchAddress("nnlo_weight_topptup", &nnlo_weight_topptup, &b_nnlo_weight_topptup);
   fChain->SetBranchAddress("nnlo_weight_ttbarptup", &nnlo_weight_ttbarptup, &b_nnlo_weight_ttbarptup);
   fChain->SetBranchAddress("nnlo_weight_exttoppt", &nnlo_weight_exttoppt, &b_nnlo_weight_exttoppt);
   fChain->SetBranchAddress("met_perp", &met_perp, &b_met_perp);
   fChain->SetBranchAddress("ttbar_m", &ttbar_m, &b_ttbar_m);
   fChain->SetBranchAddress("ttbar_pt", &ttbar_pt, &b_ttbar_pt);
   fChain->SetBranchAddress("dphi_ttbar", &dphi_ttbar, &b_dphi_ttbar);
   fChain->SetBranchAddress("dphi_leptop_met", &dphi_leptop_met, &b_dphi_leptop_met);
   fChain->SetBranchAddress("dphi_hadtop_met", &dphi_hadtop_met, &b_dphi_hadtop_met);
   fChain->SetBranchAddress("weight_top_mass_reweighting", &weight_top_mass_reweighting, &b_weight_top_mass_reweighting);
   fChain->SetBranchAddress("BDT", &BDT, &b_BDT);
   fChain->SetBranchAddress("treatAsYear", &treatAsYear, &b_treatAsYear);
   fChain->SetBranchAddress("random_run_number", &random_run_number, &b_random_run_number);
   fChain->SetBranchAddress("is_mc", &is_mc, &b_is_mc);
   fChain->SetBranchAddress("is_TT_signal", &is_TT_signal, &b_is_TT_signal);
   fChain->SetBranchAddress("has_extra_met", &has_extra_met, &b_has_extra_met);
   fChain->SetBranchAddress("TT_decay_mode", &TT_decay_mode, &b_TT_decay_mode);
   fChain->SetBranchAddress("tt_cat", &tt_cat, &b_tt_cat);
   fChain->SetBranchAddress("el_trigger", &el_trigger, &b_el_trigger);
   fChain->SetBranchAddress("mu_trigger", &mu_trigger, &b_mu_trigger);
   fChain->SetBranchAddress("xe_trigger", &xe_trigger, &b_xe_trigger);
   fChain->SetBranchAddress("stxe_trigger", &stxe_trigger, &b_stxe_trigger);
   fChain->SetBranchAddress("ph_trigger", &ph_trigger, &b_ph_trigger);
   fChain->SetBranchAddress("dilep_trigger", &dilep_trigger, &b_dilep_trigger);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VH", &HLT_e24_lhmedium_L1EM20VH, &b_HLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_g120_loose", &HLT_g120_loose, &b_HLT_g120_loose);
   fChain->SetBranchAddress("HLT_g140_loose", &HLT_g140_loose, &b_HLT_g140_loose);
   fChain->SetBranchAddress("HLT_e17_lhloose_mu14", &HLT_e17_lhloose_mu14, &b_HLT_e17_lhloose_mu14);
   fChain->SetBranchAddress("HLT_e24_lhmedium_L1EM20VHI_mu8noL1", &HLT_e24_lhmedium_L1EM20VHI_mu8noL1, &b_HLT_e24_lhmedium_L1EM20VHI_mu8noL1);
   fChain->SetBranchAddress("HLT_e7_lhmedium_mu24", &HLT_e7_lhmedium_mu24, &b_HLT_e7_lhmedium_mu24);
   fChain->SetBranchAddress("HLT_e17_lhloose_nod0_mu14", &HLT_e17_lhloose_nod0_mu14, &b_HLT_e17_lhloose_nod0_mu14);
   fChain->SetBranchAddress("HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1", &HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1, &b_HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1);
   fChain->SetBranchAddress("HLT_e7_lhmedium_nod0_mu24", &HLT_e7_lhmedium_nod0_mu24, &b_HLT_e7_lhmedium_nod0_mu24);
   fChain->SetBranchAddress("HLT_e12_lhloose_nod0_2mu10", &HLT_e12_lhloose_nod0_2mu10, &b_HLT_e12_lhloose_nod0_2mu10);
   fChain->SetBranchAddress("HLT_2e12_lhloose_nod0_mu10", &HLT_2e12_lhloose_nod0_mu10, &b_HLT_2e12_lhloose_nod0_mu10);
   fChain->SetBranchAddress("EventPassesLooseJetCleaning", &EventPassesLooseJetCleaning, &b_EventPassesLooseJetCleaning);
   fChain->SetBranchAddress("EventPassesTightJetCleaning", &EventPassesTightJetCleaning, &b_EventPassesTightJetCleaning);
   fChain->SetBranchAddress("JetWithHotCaloCells", &JetWithHotCaloCells, &b_JetWithHotCaloCells);
   fChain->SetBranchAddress("n_cosmics", &n_cosmics, &b_n_cosmics);
   fChain->SetBranchAddress("has_hadtop", &has_hadtop, &b_has_hadtop);
   fChain->SetBranchAddress("has_bjet", &has_bjet, &b_has_bjet);
   fChain->SetBranchAddress("has_ljet1", &has_ljet1, &b_has_ljet1);
   fChain->SetBranchAddress("has_ljet2", &has_ljet2, &b_has_ljet2);
   fChain->SetBranchAddress("rr_hadtop_good_cand", &rr_hadtop_good_cand, &b_rr_hadtop_good_cand);
   fChain->SetBranchAddress("rr_hadtop_good_jet", &rr_hadtop_good_jet, &b_rr_hadtop_good_jet);
   fChain->SetBranchAddress("truth_hasMEphoton", &truth_hasMEphoton, &b_truth_hasMEphoton);
   fChain->SetBranchAddress("passMEphoton", &passMEphoton, &b_passMEphoton);
   fChain->SetBranchAddress("SRpresel", &SRpresel, &b_SRpresel);
   fChain->SetBranchAddress("SR1", &SR1, &b_SR1);
   fChain->SetBranchAddress("CR1presel", &CR1presel, &b_CR1presel);
   fChain->SetBranchAddress("TCR1", &TCR1, &b_TCR1);
   fChain->SetBranchAddress("WCR1", &WCR1, &b_WCR1);
   fChain->SetBranchAddress("STCR1", &STCR1, &b_STCR1);
   fChain->SetBranchAddress("TVR1", &TVR1, &b_TVR1);
   fChain->SetBranchAddress("WVR1", &WVR1, &b_WVR1);
   fChain->SetBranchAddress("TZCR1", &TZCR1, &b_TZCR1);
   fChain->SetBranchAddress("ttZCRpresel", &ttZCRpresel, &b_ttZCRpresel);
   fChain->SetBranchAddress("tN_med", &tN_med, &b_tN_med);
   fChain->SetBranchAddress("CR_tN_med_presel", &CR_tN_med_presel, &b_CR_tN_med_presel);
   fChain->SetBranchAddress("CR_tN_med_topVeto", &CR_tN_med_topVeto, &b_CR_tN_med_topVeto);
   fChain->SetBranchAddress("T1LCR_tN_med", &T1LCR_tN_med, &b_T1LCR_tN_med);
   fChain->SetBranchAddress("T2LCR_tN_med", &T2LCR_tN_med, &b_T2LCR_tN_med);
   fChain->SetBranchAddress("WCR_tN_med", &WCR_tN_med, &b_WCR_tN_med);
   fChain->SetBranchAddress("STCR_tN_med", &STCR_tN_med, &b_STCR_tN_med);
   fChain->SetBranchAddress("TVR_tN_med", &TVR_tN_med, &b_TVR_tN_med);
   fChain->SetBranchAddress("WVR_tN_med", &WVR_tN_med, &b_WVR_tN_med);
   fChain->SetBranchAddress("TZCR_tN_med", &TZCR_tN_med, &b_TZCR_tN_med);
   fChain->SetBranchAddress("tN_high", &tN_high, &b_tN_high);
   fChain->SetBranchAddress("CR_tN_high_presel", &CR_tN_high_presel, &b_CR_tN_high_presel);
   fChain->SetBranchAddress("CR_tN_high_topVeto", &CR_tN_high_topVeto, &b_CR_tN_high_topVeto);
   fChain->SetBranchAddress("T1LCR_tN_high", &T1LCR_tN_high, &b_T1LCR_tN_high);
   fChain->SetBranchAddress("T2LCR_tN_high", &T2LCR_tN_high, &b_T2LCR_tN_high);
   fChain->SetBranchAddress("WCR_tN_high", &WCR_tN_high, &b_WCR_tN_high);
   fChain->SetBranchAddress("STCR_tN_high", &STCR_tN_high, &b_STCR_tN_high);
   fChain->SetBranchAddress("TVR_tN_high", &TVR_tN_high, &b_TVR_tN_high);
   fChain->SetBranchAddress("WVR_tN_high", &WVR_tN_high, &b_WVR_tN_high);
   fChain->SetBranchAddress("TZCR_tN_high", &TZCR_tN_high, &b_TZCR_tN_high);
   fChain->SetBranchAddress("tN_high_ICHEP", &tN_high_ICHEP, &b_tN_high_ICHEP);
   fChain->SetBranchAddress("CR_tN_high_ICHEP_presel", &CR_tN_high_ICHEP_presel, &b_CR_tN_high_ICHEP_presel);
   fChain->SetBranchAddress("TCR_tN_high_ICHEP", &TCR_tN_high_ICHEP, &b_TCR_tN_high_ICHEP);
   fChain->SetBranchAddress("WCR_tN_high_ICHEP", &WCR_tN_high_ICHEP, &b_WCR_tN_high_ICHEP);
   fChain->SetBranchAddress("STCR_tN_high_ICHEP", &STCR_tN_high_ICHEP, &b_STCR_tN_high_ICHEP);
   fChain->SetBranchAddress("TVR_tN_high_ICHEP", &TVR_tN_high_ICHEP, &b_TVR_tN_high_ICHEP);
   fChain->SetBranchAddress("WVR_tN_high_ICHEP", &WVR_tN_high_ICHEP, &b_WVR_tN_high_ICHEP);
   fChain->SetBranchAddress("TZCR_tN_high_ICHEP", &TZCR_tN_high_ICHEP, &b_TZCR_tN_high_ICHEP);
   fChain->SetBranchAddress("vlq_low", &vlq_low, &b_vlq_low);
   fChain->SetBranchAddress("vlq_high", &vlq_high, &b_vlq_high);
   fChain->SetBranchAddress("vlq_combined", &vlq_combined, &b_vlq_combined);
   fChain->SetBranchAddress("cr_vlq_presel", &cr_vlq_presel, &b_cr_vlq_presel);
   fChain->SetBranchAddress("TCR_vlq", &TCR_vlq, &b_TCR_vlq);
   fChain->SetBranchAddress("WCR_vlq", &WCR_vlq, &b_WCR_vlq);
   fChain->SetBranchAddress("TVR_vlq", &TVR_vlq, &b_TVR_vlq);
   fChain->SetBranchAddress("WVR_vlq", &WVR_vlq, &b_WVR_vlq);
   fChain->SetBranchAddress("bC2x_med", &bC2x_med, &b_bC2x_med);
   fChain->SetBranchAddress("bC2x_med_TCR", &bC2x_med_TCR, &b_bC2x_med_TCR);
   fChain->SetBranchAddress("bC2x_med_STCR", &bC2x_med_STCR, &b_bC2x_med_STCR);
   fChain->SetBranchAddress("bC2x_med_WCR", &bC2x_med_WCR, &b_bC2x_med_WCR);
   fChain->SetBranchAddress("bC2x_med_WVR", &bC2x_med_WVR, &b_bC2x_med_WVR);
   fChain->SetBranchAddress("bC2x_med_TVR", &bC2x_med_TVR, &b_bC2x_med_TVR);
   fChain->SetBranchAddress("bC2x_med_TZCR", &bC2x_med_TZCR, &b_bC2x_med_TZCR);
   fChain->SetBranchAddress("bC2x_diag", &bC2x_diag, &b_bC2x_diag);
   fChain->SetBranchAddress("bC2x_diag_TCR", &bC2x_diag_TCR, &b_bC2x_diag_TCR);
   fChain->SetBranchAddress("bC2x_diag_STCR", &bC2x_diag_STCR, &b_bC2x_diag_STCR);
   fChain->SetBranchAddress("bC2x_diag_WCR", &bC2x_diag_WCR, &b_bC2x_diag_WCR);
   fChain->SetBranchAddress("bC2x_diag_TVR", &bC2x_diag_TVR, &b_bC2x_diag_TVR);
   fChain->SetBranchAddress("bC2x_diag_WVR", &bC2x_diag_WVR, &b_bC2x_diag_WVR);
   fChain->SetBranchAddress("bC2x_diag_TZCR", &bC2x_diag_TZCR, &b_bC2x_diag_TZCR);
   fChain->SetBranchAddress("bCbv", &bCbv, &b_bCbv);
   fChain->SetBranchAddress("bCbv_TCR", &bCbv_TCR, &b_bCbv_TCR);
   fChain->SetBranchAddress("bCbv_WCR", &bCbv_WCR, &b_bCbv_WCR);
   fChain->SetBranchAddress("bCbv_TVR", &bCbv_TVR, &b_bCbv_TVR);
   fChain->SetBranchAddress("bCbv_WVR", &bCbv_WVR, &b_bCbv_WVR);
   fChain->SetBranchAddress("bCbv_SRsideband", &bCbv_SRsideband, &b_bCbv_SRsideband);
   fChain->SetBranchAddress("DM_high", &DM_high, &b_DM_high);
   fChain->SetBranchAddress("CR_DM_high_presel", &CR_DM_high_presel, &b_CR_DM_high_presel);
   fChain->SetBranchAddress("TCR_DM_high", &TCR_DM_high, &b_TCR_DM_high);
   fChain->SetBranchAddress("WCR_DM_high", &WCR_DM_high, &b_WCR_DM_high);
   fChain->SetBranchAddress("STCR_DM_high", &STCR_DM_high, &b_STCR_DM_high);
   fChain->SetBranchAddress("TVR_DM_high", &TVR_DM_high, &b_TVR_DM_high);
   fChain->SetBranchAddress("WVR_DM_high", &WVR_DM_high, &b_WVR_DM_high);
   fChain->SetBranchAddress("TZCR_DM_high", &TZCR_DM_high, &b_TZCR_DM_high);
   fChain->SetBranchAddress("DM_low", &DM_low, &b_DM_low);
   fChain->SetBranchAddress("CR_DM_low_presel", &CR_DM_low_presel, &b_CR_DM_low_presel);
   fChain->SetBranchAddress("TCR_DM_low", &TCR_DM_low, &b_TCR_DM_low);
   fChain->SetBranchAddress("WCR_DM_low", &WCR_DM_low, &b_WCR_DM_low);
   fChain->SetBranchAddress("STCR_DM_low", &STCR_DM_low, &b_STCR_DM_low);
   fChain->SetBranchAddress("TVR_DM_low", &TVR_DM_low, &b_TVR_DM_low);
   fChain->SetBranchAddress("WVR_DM_low", &WVR_DM_low, &b_WVR_DM_low);
   fChain->SetBranchAddress("TZCR_DM_low", &TZCR_DM_low, &b_TZCR_DM_low);
   fChain->SetBranchAddress("WtailVR", &WtailVR, &b_WtailVR);
   fChain->SetBranchAddress("DileptonVR", &DileptonVR, &b_DileptonVR);
   fChain->SetBranchAddress("LeptonTauVR", &LeptonTauVR, &b_LeptonTauVR);
   fChain->SetBranchAddress("amt2tailVR", &amt2tailVR, &b_amt2tailVR);
   fChain->SetBranchAddress("amt2dilresVR", &amt2dilresVR, &b_amt2dilresVR);
   fChain->SetBranchAddress("amt2dilboostVR", &amt2dilboostVR, &b_amt2dilboostVR);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("lumi_block", &lumi_block, &b_lumi_block);
   fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("SRpresel_n1", &SRpresel_n1, &b_SRpresel_n1);
   fChain->SetBranchAddress("SR1_n1", &SR1_n1, &b_SR1_n1);
   fChain->SetBranchAddress("CR1presel_n1", &CR1presel_n1, &b_CR1presel_n1);
   fChain->SetBranchAddress("TCR1_n1", &TCR1_n1, &b_TCR1_n1);
   fChain->SetBranchAddress("WCR1_n1", &WCR1_n1, &b_WCR1_n1);
   fChain->SetBranchAddress("STCR1_n1", &STCR1_n1, &b_STCR1_n1);
   fChain->SetBranchAddress("TVR1_n1", &TVR1_n1, &b_TVR1_n1);
   fChain->SetBranchAddress("WVR1_n1", &WVR1_n1, &b_WVR1_n1);
   fChain->SetBranchAddress("TZCR1_n1", &TZCR1_n1, &b_TZCR1_n1);
   fChain->SetBranchAddress("ttZCRpresel_n1", &ttZCRpresel_n1, &b_ttZCRpresel_n1);
   fChain->SetBranchAddress("tN_med_n1", &tN_med_n1, &b_tN_med_n1);
   fChain->SetBranchAddress("CR_tN_med_presel_n1", &CR_tN_med_presel_n1, &b_CR_tN_med_presel_n1);
   fChain->SetBranchAddress("CR_tN_med_topVeto_n1", &CR_tN_med_topVeto_n1, &b_CR_tN_med_topVeto_n1);
   fChain->SetBranchAddress("T1LCR_tN_med_n1", &T1LCR_tN_med_n1, &b_T1LCR_tN_med_n1);
   fChain->SetBranchAddress("T2LCR_tN_med_n1", &T2LCR_tN_med_n1, &b_T2LCR_tN_med_n1);
   fChain->SetBranchAddress("WCR_tN_med_n1", &WCR_tN_med_n1, &b_WCR_tN_med_n1);
   fChain->SetBranchAddress("STCR_tN_med_n1", &STCR_tN_med_n1, &b_STCR_tN_med_n1);
   fChain->SetBranchAddress("TVR_tN_med_n1", &TVR_tN_med_n1, &b_TVR_tN_med_n1);
   fChain->SetBranchAddress("WVR_tN_med_n1", &WVR_tN_med_n1, &b_WVR_tN_med_n1);
   fChain->SetBranchAddress("TZCR_tN_med_n1", &TZCR_tN_med_n1, &b_TZCR_tN_med_n1);
   fChain->SetBranchAddress("tN_high_n1", &tN_high_n1, &b_tN_high_n1);
   fChain->SetBranchAddress("CR_tN_high_presel_n1", &CR_tN_high_presel_n1, &b_CR_tN_high_presel_n1);
   fChain->SetBranchAddress("CR_tN_high_topVeto_n1", &CR_tN_high_topVeto_n1, &b_CR_tN_high_topVeto_n1);
   fChain->SetBranchAddress("T1LCR_tN_high_n1", &T1LCR_tN_high_n1, &b_T1LCR_tN_high_n1);
   fChain->SetBranchAddress("T2LCR_tN_high_n1", &T2LCR_tN_high_n1, &b_T2LCR_tN_high_n1);
   fChain->SetBranchAddress("WCR_tN_high_n1", &WCR_tN_high_n1, &b_WCR_tN_high_n1);
   fChain->SetBranchAddress("STCR_tN_high_n1", &STCR_tN_high_n1, &b_STCR_tN_high_n1);
   fChain->SetBranchAddress("TVR_tN_high_n1", &TVR_tN_high_n1, &b_TVR_tN_high_n1);
   fChain->SetBranchAddress("WVR_tN_high_n1", &WVR_tN_high_n1, &b_WVR_tN_high_n1);
   fChain->SetBranchAddress("TZCR_tN_high_n1", &TZCR_tN_high_n1, &b_TZCR_tN_high_n1);
   fChain->SetBranchAddress("tN_high_ICHEP_n1", &tN_high_ICHEP_n1, &b_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("CR_tN_high_ICHEP_presel_n1", &CR_tN_high_ICHEP_presel_n1, &b_CR_tN_high_ICHEP_presel_n1);
   fChain->SetBranchAddress("TCR_tN_high_ICHEP_n1", &TCR_tN_high_ICHEP_n1, &b_TCR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("WCR_tN_high_ICHEP_n1", &WCR_tN_high_ICHEP_n1, &b_WCR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("STCR_tN_high_ICHEP_n1", &STCR_tN_high_ICHEP_n1, &b_STCR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("TVR_tN_high_ICHEP_n1", &TVR_tN_high_ICHEP_n1, &b_TVR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("WVR_tN_high_ICHEP_n1", &WVR_tN_high_ICHEP_n1, &b_WVR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("TZCR_tN_high_ICHEP_n1", &TZCR_tN_high_ICHEP_n1, &b_TZCR_tN_high_ICHEP_n1);
   fChain->SetBranchAddress("vlq_low_n1", &vlq_low_n1, &b_vlq_low_n1);
   fChain->SetBranchAddress("vlq_high_n1", &vlq_high_n1, &b_vlq_high_n1);
   fChain->SetBranchAddress("vlq_combined_n1", &vlq_combined_n1, &b_vlq_combined_n1);
   fChain->SetBranchAddress("cr_vlq_presel_n1", &cr_vlq_presel_n1, &b_cr_vlq_presel_n1);
   fChain->SetBranchAddress("TCR_vlq_n1", &TCR_vlq_n1, &b_TCR_vlq_n1);
   fChain->SetBranchAddress("WCR_vlq_n1", &WCR_vlq_n1, &b_WCR_vlq_n1);
   fChain->SetBranchAddress("TVR_vlq_n1", &TVR_vlq_n1, &b_TVR_vlq_n1);
   fChain->SetBranchAddress("WVR_vlq_n1", &WVR_vlq_n1, &b_WVR_vlq_n1);
   fChain->SetBranchAddress("bC2x_med_n1", &bC2x_med_n1, &b_bC2x_med_n1);
   fChain->SetBranchAddress("bC2x_med_TCR_n1", &bC2x_med_TCR_n1, &b_bC2x_med_TCR_n1);
   fChain->SetBranchAddress("bC2x_med_STCR_n1", &bC2x_med_STCR_n1, &b_bC2x_med_STCR_n1);
   fChain->SetBranchAddress("bC2x_med_WCR_n1", &bC2x_med_WCR_n1, &b_bC2x_med_WCR_n1);
   fChain->SetBranchAddress("bC2x_med_WVR_n1", &bC2x_med_WVR_n1, &b_bC2x_med_WVR_n1);
   fChain->SetBranchAddress("bC2x_med_TVR_n1", &bC2x_med_TVR_n1, &b_bC2x_med_TVR_n1);
   fChain->SetBranchAddress("bC2x_med_TZCR_n1", &bC2x_med_TZCR_n1, &b_bC2x_med_TZCR_n1);
   fChain->SetBranchAddress("bC2x_diag_n1", &bC2x_diag_n1, &b_bC2x_diag_n1);
   fChain->SetBranchAddress("bC2x_diag_TCR_n1", &bC2x_diag_TCR_n1, &b_bC2x_diag_TCR_n1);
   fChain->SetBranchAddress("bC2x_diag_STCR_n1", &bC2x_diag_STCR_n1, &b_bC2x_diag_STCR_n1);
   fChain->SetBranchAddress("bC2x_diag_WCR_n1", &bC2x_diag_WCR_n1, &b_bC2x_diag_WCR_n1);
   fChain->SetBranchAddress("bC2x_diag_TVR_n1", &bC2x_diag_TVR_n1, &b_bC2x_diag_TVR_n1);
   fChain->SetBranchAddress("bC2x_diag_WVR_n1", &bC2x_diag_WVR_n1, &b_bC2x_diag_WVR_n1);
   fChain->SetBranchAddress("bC2x_diag_TZCR_n1", &bC2x_diag_TZCR_n1, &b_bC2x_diag_TZCR_n1);
   fChain->SetBranchAddress("bCbv_n1", &bCbv_n1, &b_bCbv_n1);
   fChain->SetBranchAddress("bCbv_TCR_n1", &bCbv_TCR_n1, &b_bCbv_TCR_n1);
   fChain->SetBranchAddress("bCbv_WCR_n1", &bCbv_WCR_n1, &b_bCbv_WCR_n1);
   fChain->SetBranchAddress("bCbv_TVR_n1", &bCbv_TVR_n1, &b_bCbv_TVR_n1);
   fChain->SetBranchAddress("bCbv_WVR_n1", &bCbv_WVR_n1, &b_bCbv_WVR_n1);
   fChain->SetBranchAddress("bCbv_SRsideband_n1", &bCbv_SRsideband_n1, &b_bCbv_SRsideband_n1);
   fChain->SetBranchAddress("DM_high_n1", &DM_high_n1, &b_DM_high_n1);
   fChain->SetBranchAddress("CR_DM_high_presel_n1", &CR_DM_high_presel_n1, &b_CR_DM_high_presel_n1);
   fChain->SetBranchAddress("TCR_DM_high_n1", &TCR_DM_high_n1, &b_TCR_DM_high_n1);
   fChain->SetBranchAddress("WCR_DM_high_n1", &WCR_DM_high_n1, &b_WCR_DM_high_n1);
   fChain->SetBranchAddress("STCR_DM_high_n1", &STCR_DM_high_n1, &b_STCR_DM_high_n1);
   fChain->SetBranchAddress("TVR_DM_high_n1", &TVR_DM_high_n1, &b_TVR_DM_high_n1);
   fChain->SetBranchAddress("WVR_DM_high_n1", &WVR_DM_high_n1, &b_WVR_DM_high_n1);
   fChain->SetBranchAddress("TZCR_DM_high_n1", &TZCR_DM_high_n1, &b_TZCR_DM_high_n1);
   fChain->SetBranchAddress("DM_low_n1", &DM_low_n1, &b_DM_low_n1);
   fChain->SetBranchAddress("CR_DM_low_presel_n1", &CR_DM_low_presel_n1, &b_CR_DM_low_presel_n1);
   fChain->SetBranchAddress("TCR_DM_low_n1", &TCR_DM_low_n1, &b_TCR_DM_low_n1);
   fChain->SetBranchAddress("WCR_DM_low_n1", &WCR_DM_low_n1, &b_WCR_DM_low_n1);
   fChain->SetBranchAddress("STCR_DM_low_n1", &STCR_DM_low_n1, &b_STCR_DM_low_n1);
   fChain->SetBranchAddress("TVR_DM_low_n1", &TVR_DM_low_n1, &b_TVR_DM_low_n1);
   fChain->SetBranchAddress("WVR_DM_low_n1", &WVR_DM_low_n1, &b_WVR_DM_low_n1);
   fChain->SetBranchAddress("TZCR_DM_low_n1", &TZCR_DM_low_n1, &b_TZCR_DM_low_n1);
   fChain->SetBranchAddress("WtailVR_n1", &WtailVR_n1, &b_WtailVR_n1);
   fChain->SetBranchAddress("DileptonVR_n1", &DileptonVR_n1, &b_DileptonVR_n1);
   fChain->SetBranchAddress("LeptonTauVR_n1", &LeptonTauVR_n1, &b_LeptonTauVR_n1);
   fChain->SetBranchAddress("amt2tailVR_n1", &amt2tailVR_n1, &b_amt2tailVR_n1);
   fChain->SetBranchAddress("amt2dilresVR_n1", &amt2dilresVR_n1, &b_amt2dilresVR_n1);
   fChain->SetBranchAddress("amt2dilboostVR_n1", &amt2dilboostVR_n1, &b_amt2dilboostVR_n1);
   fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
   fChain->SetBranchAddress("prw_hash", &prw_hash, &b_prw_hash);
   fChain->SetBranchAddress("prw_hash_tt", &prw_hash_tt, &b_prw_hash_tt);
   fChain->SetBranchAddress("xs_weight", &xs_weight, &b_xs_weight);
   Notify();
}

Bool_t STop1LBase::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void STop1LBase::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t STop1LBase::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef STop1LBase_cxx
