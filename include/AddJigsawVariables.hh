#ifndef AddJigsawVariables_h
#define AddJigsawVariables_h

#include "STop1LBase.hh"
#include "SusySkimHiggsinoBase.hh"
#include "SusySkimHiggsinov20Base.hh"
#include "LorenzoBase.hh"
#include "RestFrames/RestFrames.hh"
#include <TTree.h>
#include <TFile.h>
#include <TObject.h>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <vector>

using namespace RestFrames;
using namespace std;

class AddJigsawVariables : public SusySkimHiggsinov20Base
 {

public :

  struct Jet {
    TLorentzVector P;
    bool btag;
  };

  AddJigsawVariables( TTree* tree=0 );
  virtual ~AddJigsawVariables();
  void InitJigsawTree();
  void CleanJigsawTree();
  void AddNewBranches( TTree* tree=0);
  void ProcessFile( TString treename, TString filename );
  void ComputeJigsawVariables();
  
  TVector3 GetMET();
  void GetJets(vector<Jet>& JETs, double pt_cut, double eta_cut=-1, double btag_WP_cut=0.6459);
  void GetMuons(vector<TLorentzVector>& MUs, double pt_cut=-1, double eta_cut=-1);
  void GetElectrons(vector<TLorentzVector>& ELs, double pt_cut=-1, double eta_cut=-1);
  void GetLeptons(vector<TLorentzVector>& LEPs, vector<int>& IDs,double pt_cut=-1, double eta_cut=-1);
    
private :

  // output branches
  TBranch *m_b_nJets30;
  TBranch *m_b_nBJets;
  TBranch *m_b_PTISR;
  TBranch *m_b_RISR;
  TBranch *m_b_MS;
  TBranch *m_b_dphiISRI;
  TBranch *m_b_dphiSL1;
  TBranch *m_b_cosIL1;
  TBranch *m_b_NbV;
  TBranch *m_b_NjV;
  TBranch *m_b_NjISR;
  TBranch *m_b_NlV;
  TBranch *m_b_pTbV1;

  // common variables for output tree 
  int m_nJets30;
  int m_nBJets;
  int m_nJets;
  double m_weight;
  double m_HT;
  double m_MET;
  double m_metPhi;
  double m_dphiMin1;
  double m_dphiMin2;
  double m_dphiMin3;
  double m_dphiMinAll;
  double m_Mbb;
  double m_dphiMinbl1;
  double m_dphiMinbl2;
  double m_dphiMinbl3;
  double m_dRMinbl1;
  double m_dRMinbl2;
  double m_dRMinbl3;

  ///////////////////////
  // compressed variables
  ///////////////////////
  double m_PTISR;
  double m_PTCM;
  double m_RISR;
  double m_cosS;
  double m_MS;
  double m_ML;
  double m_MISR;
  double m_MV;
  double m_dphiCMI;
  double m_dphiISRI;
  double m_pTjV1;
  double m_pTjV2;
  double m_pTjV3;
  double m_pTjV4;
  double m_pTjV5;
  double m_pTjV6;
  double m_pTbV1;
  double m_pTbV2;
  int m_NbV;
  int m_NbISR;
  int m_NjV;
  int m_NjISR;
  int m_NlV;
  int m_NlISR;
  int m_id_1lV;
  int m_id_2lV;
  int m_id_3lV;
  int m_id_1lISR;
  int m_id_2lISR;
  int m_id_3lISR;
  double m_MW1;
  double m_MW2;
  double m_MW3;
  double m_dphiCML1;
  double m_dphiCML2;
  double m_dphiCML3;
  double m_dphiSL1;
  double m_dphiSL2;
  double m_dphiSL3;
  double m_cosIL1;
  double m_cosIL2;
  double m_cosIL3;
   
  LabRecoFrame*        LAB;
  DecayRecoFrame*      CM;
  DecayRecoFrame*      S;
  VisibleRecoFrame*    ISR;
  VisibleRecoFrame*    V;
  VisibleRecoFrame*    L;
  InvisibleRecoFrame*  I;
  InvisibleGroup*      INV;
  SetMassInvJigsaw*    InvMass;
  CombinatoricGroup*   VIS;
  MinMassesCombJigsaw* SplitVis;

};

#endif
